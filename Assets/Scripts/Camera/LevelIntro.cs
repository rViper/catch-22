﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class LevelIntro : MonoBehaviour
{
    public PostProcessingProfile levelIntroProfile;
    public PostProcessingProfile inGameProfile;

    PostProcessingBehaviour postProcessingBehaviour;
    CameraPositions cameraPositions;
    GameObject blackStripes;
    Animator myAnim;


    private void Start()
    {
        postProcessingBehaviour = this.transform.GetComponent<PostProcessingBehaviour>();
        cameraPositions = this.transform.GetComponent<CameraPositions>();
        blackStripes = this.transform.Find("BlackStripes").gameObject;
        myAnim = this.transform.GetComponent<Animator>();

        postProcessingBehaviour.profile = levelIntroProfile;
        cameraPositions.enabled = false;
    }

    public void IntroDone()
    {
        blackStripes.SetActive(false);
        myAnim.enabled = false;
        cameraPositions.enabled = true;
        postProcessingBehaviour.profile = inGameProfile;

        Player.player.canMove = true;
    }
}
