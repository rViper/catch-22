﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewCameraAngle : MonoBehaviour
{
    public float cameraFollowSpeed = 2.5f;
    public float cameraTurningSpeed = 35f;

    [Space]
    public bool targetThisCamera;
    public Vector3 newAngle;
    public Vector3 targetOffset;
    public float cameraDepth;

    [Space]
    public bool postProcessingEffects;
    public float bloomIntensity;
    public float vignetteIntensity;
    public float aperture;
    public float focalLength;

    CameraPositions mainCamera;

    void Start()
    {
        mainCamera = Camera.main.GetComponent<CameraPositions>();
    }

    public void StartCamera()
    {
        if (targetThisCamera)
            mainCamera.SetNewPosition(newAngle, this.transform, targetOffset, cameraDepth, cameraFollowSpeed, cameraTurningSpeed);
        else
            mainCamera.SetNewPosition(newAngle, targetOffset, cameraDepth, cameraFollowSpeed, cameraTurningSpeed);

        if (postProcessingEffects)
            mainCamera.SetNewPostEffect(bloomIntensity, vignetteIntensity, aperture, focalLength);
    }

    public void FinishCamera()
    {
        mainCamera.SetNormalPosition();
    }

    void OnTriggerEnter2D(Collider2D player)
    {
        if (player.tag == "Player")
            StartCamera();
    }

    void OnTriggerExit2D(Collider2D player)
    {
        if (player.tag == "Player")
            FinishCamera();
    }
}
