﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraPositions : MonoBehaviour {

    float cameraFollowSpeed;
    float cameraTurningSpeed;

    Transform mainCamera;
    public Transform player;
    public Transform target;

    Vector3 targetPosition;
    Quaternion targetRotation;
    float depthValue;
    Vector3 offSetValue;

    int triggerCounter;

    PostProcessingProfile postProfile;
    public PostProcessingProfile originalSettings;

	void Start ()
    {
        mainCamera = Camera.main.transform;
        player = GameObject.FindWithTag("Player").transform;
        target = player;

        targetRotation = Quaternion.identity;
        depthValue = mainCamera.position.z;

        postProfile = this.gameObject.GetComponent<PostProcessingBehaviour>().profile;

        postProfile.bloom.settings = originalSettings.bloom.settings;
        postProfile.vignette.settings = originalSettings.vignette.settings;
        postProfile.depthOfField.settings = originalSettings.depthOfField.settings;
    }
	
	void Update ()
    {
        if (target !=null)
        {
            targetPosition = new Vector3(target.position.x + offSetValue.x, target.position.y + offSetValue.y, depthValue + offSetValue.z);

            mainCamera.transform.position = Vector3.Lerp(mainCamera.position, targetPosition, cameraFollowSpeed * Time.deltaTime);
            mainCamera.transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, cameraTurningSpeed * Time.deltaTime);
        }
    }

    public void SetNewPosition(Vector3 _newAngle, Vector3 _offsetValue, float _depthValue, float _cameraFollowSpeed, float _cameraTurningSpeed)
    {
        targetRotation = Quaternion.Euler(_newAngle);
        offSetValue = _offsetValue;
        depthValue = _depthValue;
        target = player;
        cameraFollowSpeed = _cameraFollowSpeed;
        cameraTurningSpeed = _cameraTurningSpeed;

        triggerCounter++;
    }

    public void SetNewPosition(Vector3 _newAngle, Transform _target, Vector3 _offsetValue, float _depthValue, float _cameraFollowSpeed, float _cameraTurningSpeed)
    {
        targetRotation = Quaternion.Euler(_newAngle);
        target = _target;
        offSetValue = _offsetValue;
        depthValue = _depthValue;
        cameraFollowSpeed = _cameraFollowSpeed;
        cameraTurningSpeed = _cameraTurningSpeed;

        triggerCounter++;
    }

    public void SetNewPostEffect(float bloomIntensity, float vignetteIntensity, float aperture, float focalLength)
    {
        BloomModel.Settings bloomSettings = postProfile.bloom.settings;
        VignetteModel.Settings vignetteSettings = postProfile.vignette.settings;
        DepthOfFieldModel.Settings depthSettings = postProfile.depthOfField.settings;

        bloomSettings.bloom.intensity = bloomIntensity;
        vignetteSettings.intensity = vignetteIntensity;
        depthSettings.aperture = aperture;
        depthSettings.focalLength = focalLength;

        postProfile.depthOfField.settings = depthSettings;
    }

    public void SetNormalPosition()
    {
        triggerCounter--;

        if (triggerCounter == 0)
        {
            targetRotation = Quaternion.identity;
            target = player;
            offSetValue = new Vector3(0, 0, 0);
            depthValue = -30;
            cameraFollowSpeed = 2.5f;
        }

        postProfile.bloom.settings = originalSettings.bloom.settings;
        postProfile.vignette.settings = originalSettings.vignette.settings;
        postProfile.depthOfField.settings = originalSettings.depthOfField.settings;
    }

}
