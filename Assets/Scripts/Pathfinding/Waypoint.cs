﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Waypoint : MonoBehaviour
{
    public bool available = true;
    public bool teleportPoint = false;

    public List<Waypoint> neighbors;
    public Color lineColor = Color.red;

    public Waypoint previous { get; set; }
    public float distance { get; set; }

    [Space]
    public bool onlySelected = false; 


    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 0.25f);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 0.6f);

        if (neighbors != null)
        {
            Gizmos.color = Color.cyan;

            foreach (var neighbor in neighbors)
            {
                if (neighbor != null)
                    Gizmos.DrawLine(transform.position, neighbor.transform.position);
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(transform.position, 0.25f);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, 0.6f);

        if (!onlySelected)
        {
            if (neighbors != null)
            {
                Gizmos.color = lineColor;

                foreach (var neighbor in neighbors)
                {
                    if (neighbor != null)
                        Gizmos.DrawLine(transform.position, neighbor.transform.position);
                }
            }
        }
    }
}