﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Pathfinding
{
    public static Stack<Vector3> NavigateTo(Vector3 closestGoalNode, Vector3 entityPosition)
    {
        // A*
        Stack<Vector3> followingPath = new Stack<Vector3>();

        Waypoint actualNode = FindClosestNode(entityPosition);
        Waypoint goal = FindClosestNode(closestGoalNode);

        SortedList<float, Waypoint> frontier = new SortedList<float, Waypoint>();
        frontier.Add(0, actualNode);

        List<Waypoint> came_from = new List<Waypoint>();

        actualNode.previous = null;
        actualNode.distance = 0;        // Para evitarnos que pete cuando comprobemos mas adelante distancias.

        while (frontier.Count > 0)
        {
            actualNode = frontier.Values[0];
            frontier.RemoveAt(0);

            // If the node is available || Destructible walls will have an unavailable node and it will become available the moment the wall its gone.
            if (actualNode.available)
            {
                float distance = actualNode.distance;

                came_from.Add(actualNode);

                //Early Exit
                if (actualNode == goal)
                    break;

                foreach (Waypoint neighbor in actualNode.neighbors)
                {
                    if (came_from.Contains(neighbor) || frontier.ContainsValue(neighbor))
                        continue;   // Break statement; will go for the next foreach.

                    neighbor.previous = actualNode;
                    neighbor.distance = distance + (neighbor.transform.position - actualNode.transform.position).magnitude;
                    float distanceToTarget = (neighbor.transform.position - goal.transform.position).magnitude;

                    frontier.Add(neighbor.distance + distanceToTarget, neighbor);
                }
            }
        }

        // Recreate the path
        if (actualNode == goal)
        {
            while (actualNode.previous != null)
            {
                followingPath.Push(actualNode.transform.position);
                actualNode = actualNode.previous;
            }

            // Finally we add our current position to the stack at the start position
            // ****** CAMBIAR LUEGO PARA QUE SEA LA POSICION DE LA ENTIDAD, PARA QUE NO DEPENDA DE EL AGENTE DONDE ESTE METIDO EL SCRIPT COMO AHORA *******
            followingPath.Push(entityPosition);

            return followingPath;
        }

        return followingPath;
    }

    public static Waypoint FindClosestNode(Vector3 closestPosition)
    {
        GameObject closestNode = null;
        float closestDistance = Mathf.Infinity;


        // This could be redonde in the future so it doesn't have to check every waypoint in the scene.
        foreach (GameObject waypoint in GameObject.FindGameObjectsWithTag("Waypoint"))
        {
            float distance = (waypoint.transform.position - closestPosition).magnitude;

            if (distance < closestDistance)
            {
                closestNode = waypoint;
                closestDistance = distance;
            }
        }

        // NOTE || The function should always return something unless THERE ARE NO WAYPOINTS on the scene. 
        //      || In case of returning NULL check waypoints TAG.
        // If a waypoint has been found it will return the closest waypoint to the entity

        if (closestNode != null)
            return closestNode.GetComponent<Waypoint>();
        else
            return null;
    }
}
