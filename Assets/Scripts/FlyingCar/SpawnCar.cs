﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCar : MonoBehaviour
{
    public GameObject[] cars = new GameObject[6];

    private float timer = 0;

    void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0f)
        {
            GameObject newCar = Instantiate(cars[Random.Range(0, 4)], new Vector3 (transform.position.x, transform.position.y + Random.Range(-6.0f, 6.0f), transform.position.z + Random.Range(-4.0f, 4.0f)), transform.rotation) as GameObject;

            timer = Random.Range(0.5f, 7.0f);
        }
    }
}
