﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingCar : MonoBehaviour
{
    public int direction = 1;       // +1 - Right | -1 - Left
    public float speed = 0.15f;

    Vector3 newPosition;

    private void Start()
    {
        Destroy(this.gameObject, 30);
    }

    private void Update()
    {
        newPosition = transform.position;

        newPosition.x = this.transform.position.x + (speed * direction);
        newPosition.y += (Mathf.Sin(Time.time) * Time.deltaTime) * 0.15f;

        transform.position = newPosition;
    }
}
