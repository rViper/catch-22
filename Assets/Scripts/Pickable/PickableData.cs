﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableData : MonoBehaviour
{
    public string message;

    private bool onPosition = false;
    AudioSource pickSound;

    void Start()
    {
        pickSound = this.GetComponent<AudioSource>();
    }

    void Update()
    {
        // Interactuable (Weapons | Covers)
        if (Input.GetKeyDown(KeyCode.E) && onPosition)
        {
            PickUpItem();

            foreach (Transform child in transform)
                child.gameObject.SetActive(false);

            Destroy(this.gameObject, 5);
        }
    }

    void PickUpItem()
    {
        GameMaster.GM.SetDataAcquried(message, pickSound, true);
    }

    void OnTriggerEnter2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {

            onPosition = true;
        }
    }

    void OnTriggerExit2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            onPosition = false;
        }
    }
}