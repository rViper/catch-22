﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRotation : MonoBehaviour {

    public Transform shoulder;

    [Space]
    public float rotationOffeset = 90;
    public float speedRotation = 8;
    public float idleAngle = -88;
    public float angleFlip = 100;
    bool rotate;

    Vector3 mousePosition;


    void LateUpdate ()
    {
        /*
        if (rotate)
        {
            Vector3 mousePosition = Input.mousePosition;
            Vector3 difference = Camera.main.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, transform.position.z - Camera.main.transform.position.z));

            float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

            transform.rotation = Quaternion.Euler(0f, 0f, rotationOffeset + rotZ);
        }

        else if (!rotate && (transform.position.z != idleAngle))
        {
            transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 0, idleAngle * transform.parent.localScale.x), speedRotation * Time.deltaTime);
        }
        */

        
        if (rotate)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawLine(ray.origin, ray.direction * 100, Color.yellow);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                mousePosition = hit.point;
            }

            Vector2 direction = mousePosition - shoulder.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            shoulder.rotation = Quaternion.Euler(0f, 0f, rotationOffeset + angle);

            /*
            if (angle * transform.parent.localScale.x >= angleFlip || angle * transform.parent.localScale.x <= -angleFlip)
            {
                transform.parent.GetComponent<PlayerController>().Flip();
            }
            */
        }


        //else if (!rotate && (transform.position.z != idleAngle))
        //{
        //    transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 0, idleAngle * transform.parent.localScale.x), speedRotation * Time.deltaTime);
        //}
        

        // 2D Rotation

        /*
        if (rotate)
        {
            Vector3 difference = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized * transform.parent.localScale.x;

            Debug.Log(difference);

            float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

            Debug.Log(rotZ);

            transform.rotation = Quaternion.Euler(0f, 0f, rotZ);

            if (rotZ * transform.parent.localScale.x >= angleFlip || rotZ * transform.parent.localScale.x <= -angleFlip)
            {
                transform.parent.GetComponent<PlayerController>().Flip();
            }

        }
        else if (!rotate && (transform.position.z != idleAngle))
        {
            transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 0, idleAngle * transform.parent.localScale.x), speedRotation * Time.deltaTime);
        }
        */
    }

    public void Rotation (bool _rotate)
    {
        rotate = _rotate;
    }
}
