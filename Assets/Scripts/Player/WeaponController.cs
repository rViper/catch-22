﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponController : MonoBehaviour
{
    public Transform weaponHold;
    public GameObject[] weaponBelt = new GameObject[2];         // Array with the different weapons available to the player;

    void Start()
    {
        if (weaponBelt[0] != null)
        {
            GameObject instantiedWeapon = Instantiate(weaponBelt[0], weaponHold.position, weaponHold.rotation) as GameObject;
            instantiedWeapon.transform.parent = weaponHold;
            weaponBelt[0] = instantiedWeapon;

            // UI Draw
            if (weaponBelt[0].GetComponent<Gun>() != null)
            {
                GameObject.Find("HUD/Canvas/Ammo/PRIMARY").GetComponent<Text>().text = weaponBelt[0].GetComponent<Gun>().transform.name;
                GameObject.Find("HUD/Canvas/Ammo/PRIMARY").GetComponent<Text>().color = new Vector4(1f, 0.9f, 0.2f, 1f);
                GameObject.Find("HUD/Canvas/Ammo/primaryWeapon").GetComponent<Text>().text = "Ammo: " + weaponBelt[0].GetComponent<Gun>().ammo.ToString();
            }
            else
            {
                GameObject.Find("HUD/Canvas/Ammo/PRIMARY").GetComponent<Text>().text = weaponBelt[0].GetComponent<Throwable>().transform.name;
                GameObject.Find("HUD/Canvas/Ammo/primaryWeapon").GetComponent<Text>().text = weaponBelt[0].GetComponent<Throwable>().animationName;
            }
        }
        if (weaponBelt[1] != null)
        {
            GameObject instantiedWeapon = Instantiate(weaponBelt[1], weaponHold.position, weaponHold.rotation) as GameObject;
            instantiedWeapon.transform.parent = weaponHold;
            weaponBelt[1] = instantiedWeapon;
            weaponBelt[1].SetActive(false);

            // UI Draw
            if (weaponBelt[1].GetComponent<Gun>() != null)
            {
                GameObject.Find("HUD/Canvas/Ammo/SECONDARY").GetComponent<Text>().text = weaponBelt[1].GetComponent<Gun>().transform.name;
                GameObject.Find("HUD/Canvas/Ammo/secondaryWeapon").GetComponent<Text>().text = "Ammo: " + weaponBelt[1].GetComponent<Gun>().ammo.ToString();
            }
            else
            {
                GameObject.Find("HUD/Canvas/Ammo/SECONDARY").GetComponent<Text>().text = weaponBelt[1].GetComponent<Throwable>().transform.name;
                GameObject.Find("HUD/Canvas/Ammo/secondaryWeapon").GetComponent<Text>().text = weaponBelt[1].GetComponent<Throwable>().animationName;
            }
        }
    }

    public void EquipWeapon(GameObject gunToEquip, int beltPosition)
    {
        GameObject instantiedWeapon = Instantiate(gunToEquip.GetComponent<Weapon>().droppableSpawnable, weaponHold.position, weaponHold.rotation) as GameObject;

        // If the weapon is a gun and not a melee weapon ( Knife | Axe )
        if (instantiedWeapon.GetComponent<Gun>())
        {
            instantiedWeapon.GetComponent<Gun>().ammo = gunToEquip.GetComponent<Gun>().ammo;  // Now the weapon that is beign dropped got the same bullets as the one the enemy was usign.
        }

        instantiedWeapon.transform.parent = weaponHold;

        Debug.Log(weaponBelt[0]);
        Debug.Log(weaponBelt[1]);

        if (weaponBelt[0] == null)
        {
            Debug.Log("if - 1");
            weaponBelt[0] = instantiedWeapon;

            if (weaponBelt[1] != null)
                weaponBelt[1].SetActive(false);
        }
        else if(weaponBelt[1] == null)
        {
            Debug.Log("else if - 2");
            weaponBelt[1] = instantiedWeapon;

            if (weaponBelt[0] != null)
                weaponBelt[0].SetActive(false);
        }
        else
        {
            Destroy(weaponBelt[beltPosition].gameObject);
            weaponBelt[beltPosition] = instantiedWeapon;
        }

        // UI Draw
        if (weaponBelt[beltPosition].GetComponent<Gun>() != null)
        {
            if (beltPosition == 0)
                GameObject.Find("HUD/Canvas/Ammo/primaryWeapon").GetComponent<Text>().text = "Ammo: " + weaponBelt[0].GetComponent<Gun>().ammo.ToString();
            else
                GameObject.Find("HUD/Canvas/Ammo/secondaryWeapon").GetComponent<Text>().text = "Ammo: " + weaponBelt[1].GetComponent<Gun>().ammo.ToString();
        } 
        else
        {
            if (beltPosition == 0)
                GameObject.Find("HUD/Canvas/Ammo/primaryWeapon").GetComponent<Text>().text = weaponBelt[0].GetComponent<Throwable>().animationName;
            else
                GameObject.Find("HUD/Canvas/Ammo/secondaryWeapon").GetComponent<Text>().text = weaponBelt[1].GetComponent<Throwable>().animationName;
        }

        DrawUI();

        Destroy(gunToEquip);
    }	

    public bool SwapWeapon (int scrollWheelValue)
    {
        Debug.Log(scrollWheelValue);
        if (weaponBelt[scrollWheelValue] != null && !weaponBelt[scrollWheelValue].activeSelf)
        {
            if (scrollWheelValue == 0)
            {
                weaponBelt[0].SetActive(true);
                if (weaponBelt[1] != null)
                    weaponBelt[1].SetActive(false);

                // UI DRAW
                if (weaponBelt[scrollWheelValue].GetComponent<Gun>() != null)
                    GameObject.Find("HUD/Canvas/Ammo/PRIMARY").GetComponent<Text>().text = weaponBelt[0].GetComponent<Gun>().transform.name;
                else
                    GameObject.Find("HUD/Canvas/Ammo/PRIMARY").GetComponent<Text>().text = weaponBelt[0].GetComponent<Throwable>().transform.name;

                GameObject.Find("HUD/Canvas/Ammo/PRIMARY").GetComponent<Text>().color = new Vector4(1f, 0.9f, 0.2f, 1f);
                GameObject.Find("HUD/Canvas/Ammo/SECONDARY").GetComponent<Text>().color = new Vector4(1, 1, 1, 1);

            }
            else
            {
                weaponBelt[1].SetActive(true);
                if (weaponBelt[0] != null)
                    weaponBelt[0].SetActive(false);

                // UI DRAW
                if (weaponBelt[scrollWheelValue].GetComponent<Gun>() != null)
                    GameObject.Find("HUD/Canvas/Ammo/SECONDARY").GetComponent<Text>().text = weaponBelt[1].GetComponent<Gun>().transform.name;
                else
                    GameObject.Find("HUD/Canvas/Ammo/SECONDARY").GetComponent<Text>().text = weaponBelt[1].GetComponent<Throwable>().transform.name;

                GameObject.Find("HUD/Canvas/Ammo/PRIMARY").GetComponent<Text>().color = new Vector4(1, 1, 1, 1);
                GameObject.Find("HUD/Canvas/Ammo/SECONDARY").GetComponent<Text>().color = new Vector4(1f, 0.9f, 0.2f, 1f);

            }

            return true;
        }

        return false;
    }

    // Called when the player is going to use one of the gadgets
    public void LockWeapons(bool gadgetActivity)
    {
        if (gadgetActivity)
        {
            if (weaponBelt[0] != null)
                weaponBelt[0].SetActive(false);
            if (weaponBelt[1] != null)
                weaponBelt[1].SetActive(false);
        }
        else
        {
            if (weaponBelt[0] != null)
                weaponBelt[0].SetActive(true);
            else if (weaponBelt[1] != null)
                weaponBelt[1].SetActive(true);
        }
        
    }

    // If the a weapon selected contains the interface IShoot we will shoot with it.
    // If contains IThrow we are going to throw it (Knife, Axe, Hammer...)
    public void Shoot(int beltNumber, ShootingSoundManager soundManager)
    {
        if (weaponBelt[beltNumber] != null)
        {
            //equippedGun.Shoot();

            IShoot holdTrigger = weaponBelt[beltNumber].GetComponent<IShoot>();

            if (holdTrigger != null)
            {
                holdTrigger.Shoot(soundManager);


                // UI Draw
                if (beltNumber == 0)
                    GameObject.Find("HUD/Canvas/Ammo/primaryWeapon").GetComponent<Text>().text = "Ammo: " + weaponBelt[beltNumber].GetComponent<Gun>().ammo.ToString();
                else
                    GameObject.Find("HUD/Canvas/Ammo/secondaryWeapon").GetComponent<Text>().text = "Ammo: " + weaponBelt[beltNumber].GetComponent<Gun>().ammo.ToString(); 
            }

            else
            {
                IThrow throwWeapon = weaponBelt[beltNumber].GetComponent<IThrow>();

                if (throwWeapon != null)
                {
                    throwWeapon.Throw();

                    Destroy(weaponBelt[beltNumber].gameObject);
                    weaponBelt[beltNumber] = null;
                }
            }
        }
    }

    public string Melee(int beltNumber)
    {
        string animationName = "Generic";

        if (weaponBelt[beltNumber] != null)
        {
            IMelee meleeWeapon = weaponBelt[beltNumber].GetComponent<IMelee>();

            if(meleeWeapon != null)
            {
                animationName = meleeWeapon.Melee();
            }
        }

        return animationName;
    }

    public void DrawUI()
    {
        // UI Draw
        if (weaponBelt[1] != null)
        {
            if (weaponBelt[1].GetComponent<Gun>() != null)
            {
                GameObject.Find("HUD/Canvas/Ammo/SECONDARY").GetComponent<Text>().text = weaponBelt[1].GetComponent<Gun>().transform.name;
                GameObject.Find("HUD/Canvas/Ammo/secondaryWeapon").GetComponent<Text>().text = "Ammo: " + weaponBelt[1].GetComponent<Gun>().ammo.ToString();
            }
            else
            {
                GameObject.Find("HUD/Canvas/Ammo/SECONDARY").GetComponent<Text>().text = weaponBelt[1].GetComponent<Throwable>().transform.name;
                GameObject.Find("HUD/Canvas/Ammo/secondaryWeapon").GetComponent<Text>().text = weaponBelt[1].GetComponent<Throwable>().animationName;
            }
        }

        // UI Draw
        if (weaponBelt[0] != null)
        {
            if (weaponBelt[0].GetComponent<Gun>() != null)
            {
                GameObject.Find("HUD/Canvas/Ammo/PRIMARY").GetComponent<Text>().text = weaponBelt[0].GetComponent<Gun>().transform.name;
                GameObject.Find("HUD/Canvas/Ammo/primaryWeapon").GetComponent<Text>().text = "Ammo: " + weaponBelt[0].GetComponent<Gun>().ammo.ToString();
            }
            else
            {
                GameObject.Find("HUD/Canvas/Ammo/PRIMARY").GetComponent<Text>().text = weaponBelt[0].GetComponent<Throwable>().transform.name;
                GameObject.Find("HUD/Canvas/Ammo/primaryWeapon").GetComponent<Text>().text = weaponBelt[0].GetComponent<Throwable>().animationName;
            }
        }
    }
}
