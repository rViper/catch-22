﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PlayerController : MonoBehaviour {

    Rigidbody2D myRigidbody;

    bool facingRight = true;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        myRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    public void Move(Vector2 velocity)
    {
        //myRigidbody.MovePosition(myRigidbody.position + velocity * Time.deltaTime);
        myRigidbody.velocity = new Vector2(velocity.x, myRigidbody.velocity.y);
        
        if (velocity.x > 0 && !facingRight) Flip();         // If velocity.x is > 0 means the player is moving right
        else if (velocity.x < 0 && facingRight) Flip();     // If velocity.x is < 0 means the player is moving left
        
    }

    public void Flip()      
    {
        /*
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;
        // Multiply the player's x local scale by -1.
        Vector2 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        */

        
        // Transform the Y rotation depending on which direction is the player going.
        Quaternion localRotation = transform.localRotation;

        if (facingRight) localRotation.y = 180;
        if (!facingRight) localRotation.y = 0;

        transform.localRotation = localRotation;

        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;
        GetComponent<Player>().facingRight = facingRight;
    }
}
