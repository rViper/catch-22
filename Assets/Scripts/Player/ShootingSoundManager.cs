﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ShootingSoundManager : MonoBehaviour
{
    public List<GameObject> enemiesInRange = new List<GameObject>();

    private CircleCollider2D triggerSound;


    void Start()
    {
        triggerSound = this.GetComponent<CircleCollider2D>();
    }

    public void SoundTriggerSize(float multiplier)
    {
        triggerSound.radius = 1 * multiplier;
    }

    public void AlertAgents(float multiplier)
    {
        SoundTriggerSize(multiplier);
        //enemiesInRange.ForEach(GetComponent<Enemy>().SoundAlert(new Vector3(0, 0, 0));

        for (int i = 0; i < enemiesInRange.Count; i++)
        {
            enemiesInRange[i].GetComponent<Enemy>().SoundAlert(this.transform.position);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag.Equals("Enemy"))
            enemiesInRange.Add(collider.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag.Equals("Enemy"))
            try
            {
                enemiesInRange.Remove(collider.gameObject);
            }
            catch (IOException e)
            {
            }
    }
}
