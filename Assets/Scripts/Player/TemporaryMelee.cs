﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporaryMelee : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            IDamagable damagableObjet = collision.gameObject.GetComponentInParent<IDamagable>();

            if (damagableObjet != null)
            {
                Debug.Log("Enemy");

                damagableObjet.TakeDamage(1, new Vector2(1, 0), collision.gameObject);
            }
        }
    }
}
