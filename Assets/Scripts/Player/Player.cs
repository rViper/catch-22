﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

[RequireComponent(typeof(PlayerController))]
public class Player : Entity
{
    public static Player player;

    public float moveSpeed = 5;
    public bool movement = true;
    public bool crouching = false;

    public float dashDrag;

    PlayerController controller;
    //WeaponController weaponController;
    HookController hookController;
    GadgetController gadgetController;
    ArmRotation armRotator;
    ArmRotation headRotator;
    Animator anim;

    private bool hookOut = true;
    public bool facingRight = true;

    [Space]
    [Space]
    public Material mat;
    public GameObject temporaryTrigger;

    public bool optionsEnabled = false;
    public bool canMove = false;

    //SOUNDS
    FMOD.Studio.EventInstance walkingSound;

    private void Awake()
    {
        if (player != null)
            Destroy(this.gameObject);
        else
            player = this;
    }

    protected override void Start ()
    {
        base.Start();

        controller = GetComponent<PlayerController>();
        //weaponController = GetComponent<WeaponController>();
        hookController = GetComponent<HookController>();
        gadgetController = GetComponent<GadgetController>();
        armRotator = this.transform.GetComponent<ArmRotation>();
        //headRotator = transform.Find("Head").GetComponent<ArmRotation>();
        anim = this.transform.GetComponentInChildren<Animator>();
    }

    private void FixedUpdate()
    {
        if (!optionsEnabled && canMove)
        {
            // Movement Input
            if (movement)
            {
                Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), 0).normalized;
                Vector2 moveVelocity = moveInput * moveSpeed;

                // Dash Input
                if (Input.GetKey(KeyCode.LeftControl) && moveVelocity.x > (moveVelocity.x / 1.2f))
                {
                    Debug.Log("Dash");

                }

                controller.Move(moveVelocity);

                anim.SetFloat("Velocity", Mathf.Abs(moveVelocity.x));

                if (moveInput.x != 0 && !SoundManager.Instance.isPlaying(walkingSound))
                {
                    //walkingSound = SoundManager.Instance.PlayOneShotSound("event:/FX/Misc/andarIntetior", transform.position);
                }
                else
                {
                    //walkingSound.stop(STOP_MODE.IMMEDIATE);
                }
            }
        }
    }

    public override void Die(Vector2 vector2, GameObject nothing)
    {
        //base.Die(vector2, nothing);

        GameMaster.GM.RespawnPlayer(this.gameObject);
    }

    void Update ()
    {
        if (!optionsEnabled && canMove)
        {
            // Crouch
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                anim.SetBool("Crouch", true);
                crouching = true;
                moveSpeed = 2;
            }

            if (Input.GetKeyUp(KeyCode.LeftControl))
            {
                anim.SetBool("Crouch", false);
                crouching = false;
                moveSpeed = 5;
            }

            // Activate Gadget
            // In the future pressing the Q key will show a radial menu that will let you select one of the gadgets you have. Doom like style for the weapon.
            //if (Input.GetKeyDown(KeyCode.Q))
            //{
            //    hookOut = !hookOut;

            //    gadgetController.ActivateGadget(hookOut);
            //    hookController.ActivateHook(hookOut);
            //}

            // Swap between traps
            // Scroll Up
            if (Input.GetAxisRaw("Mouse ScrollWheel") > 0)
            {
                // HUD

                // Gadget
                //gadgetController.SwapGadget(true, hookOut);
                gadgetController.SwapGadget(true, false);

                //if (weaponController.SwapWeapon(0))
                //weaponEquipped = 0;
            }
            // Scroll Down
            else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0)
            {
                //if (weaponController.SwapWeapon(1))
                //    weaponEquipped = 1;

                //gadgetController.SwapGadget(false, hookOut);
                gadgetController.SwapGadget(false, false);
                //weaponEquipped = 1;
            }

            else if (Input.GetKeyDown(KeyCode.Alpha1))
                gadgetController.SpecificGadget(0);

            else if (Input.GetKeyDown(KeyCode.Alpha2))
                gadgetController.SpecificGadget(1);

            else if (Input.GetKeyDown(KeyCode.Alpha3))
                gadgetController.SpecificGadget(2);

            else if (Input.GetKeyDown(KeyCode.Alpha4))
                gadgetController.SpecificGadget(3);


            // Shooting Input
            if (Input.GetMouseButton(1))
            {
                armRotator.Rotation(true);

                if (Input.GetMouseButtonDown(0))
                {
                    //if (hookOut)
                    //    hookController.UseHook();
                    //else
                        gadgetController.UseGadget(facingRight);
                }
            }
            else if (Input.GetMouseButtonUp(1))
            {
                //headRotator.Rotation(false);
                armRotator.Rotation(false);
                //movement = true;
            }


            else if (Input.GetMouseButtonDown(0))
            {
                canMove = false;
                controller.Move(new Vector2());
                anim.Play("Melee");
                anim.SetBool("Crouch", false);
                crouching = false;
                moveSpeed = 5;
                StartCoroutine(TemporaryMeleeAtack(1.2f));
            }
        }
    }

    private IEnumerator TemporaryMeleeAtack(float waitTime)
    {
        temporaryTrigger.SetActive(true);
        //mat.color = new Color(1, 1, 0);

        SoundManager.Instance.PlayOneShotSound("event:/FX/Player/Melee", transform.position);

        yield return new WaitForSeconds(waitTime);

        canMove = true;

        temporaryTrigger.SetActive(false);
        //mat.color = new Color(0, 0.8f, 1);
        
    }
}
