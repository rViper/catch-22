﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleWall : MonoBehaviour, IDestructible
{
    public GameObject prefabReplace;

    public void ReplacePrefab()
    {
        GameObject newWall = Instantiate(prefabReplace, this.transform.position, this.transform.rotation) as GameObject;

        Destroy(this.gameObject);
    }
}
