﻿using UnityEngine;

public interface IDamagable
{
    void TakeDamage(int damage, Vector2 direction, GameObject bodyPart);
}
