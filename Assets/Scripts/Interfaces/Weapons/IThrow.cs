﻿using UnityEngine;

public interface IThrow
{
    void Throw();
}
