﻿using UnityEngine;

public interface IShoot
{
    void Shoot();
    void Shoot(ShootingSoundManager soundManager);
}
