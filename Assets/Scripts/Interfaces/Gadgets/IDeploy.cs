﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDeploy
{
    // false - Ground gadgets | Rigidbody addforce
    // true - Wall gadgets | Check distance from player to wall, then attach the gadget to the wall. 
    void Deploy(int direction);    
}
