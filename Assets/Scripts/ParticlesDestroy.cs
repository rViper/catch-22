﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesDestroy : MonoBehaviour
{
    private ParticleSystem particle;
    private AudioSource audio;


    public void Start()
    {
        particle = GetComponent<ParticleSystem>();
        audio = GetComponent<AudioSource>();

        audio.Play();
    }

    public void Update()
    {
        if (particle)
        {
            if (!particle.IsAlive())
            {
                Destroy(gameObject);
            }
        }
    }
}
