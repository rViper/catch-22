﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

enum CinematicState
{
    CINEMATIC,
    PRESSTOSKIP
}

public class Director : MonoBehaviour
{
    GameObject pressToSkip;
    Text pressToSkipText;

    [SerializeField]
    CinematicState actualState;

    public float timer = 0;

    [Space]
    public int levelToLoad;

    private void Awake()
    {
        pressToSkip = GameObject.Find("PressToSkipCanvas");
        pressToSkipText = GameObject.Find("PressToSkip").GetComponent<Text>();
        actualState = CinematicState.CINEMATIC;

        pressToSkip.SetActive(false);
    }

    void Update()
    {
        if (Input.anyKeyDown && actualState == CinematicState.CINEMATIC)
        {
            pressToSkip.SetActive(true);
            actualState = CinematicState.PRESSTOSKIP;
            pressToSkipText.color = new Color(pressToSkipText.color.r, pressToSkipText.color.g, pressToSkipText.color.b, 1);

            timer = 0;
        }
        else if (Input.anyKeyDown && actualState == CinematicState.PRESSTOSKIP)
        {
            LoadLevel();
        }

        if (actualState == CinematicState.PRESSTOSKIP)
        {
            if (timer > 3 && pressToSkipText.color.a != 0)
            {
                pressToSkipText.color = new Color(pressToSkipText.color.r, pressToSkipText.color.g, pressToSkipText.color.b, pressToSkipText.color.a - 0.025f);
            }
            if (timer > 3 && pressToSkipText.color.a <= 0)
            {
                Debug.Log("STATE");
                actualState = CinematicState.CINEMATIC; 
            }
        }

        timer += Time.deltaTime;
    }

    void LoadLevel()
    {
        SceneManager.LoadScene(levelToLoad, LoadSceneMode.Single);
    }
}
