﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailMovement : MonoBehaviour {

    public float moveSpeed = 5;

	void Update ()
    {
        transform.Translate(Vector3.right * Time.deltaTime * moveSpeed);
	}
}
