﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeThrow : MonoBehaviour {

    Rigidbody2D rb2D;
    int damage;

    void Awake()
    {
        rb2D = this.gameObject.GetComponent<Rigidbody2D>();
    }

    public void setThrowableProperties(float thrustForce, float torqueSpin, int _damage)
    {
        damage = _damage;
        //rb2D.AddForce(transform.right * thrustForce);
        rb2D.AddForce(transform.right * thrustForce, ForceMode2D.Impulse);

        Debug.Log("TORQUE");

        rb2D.AddTorque(torqueSpin, ForceMode2D.Force);
    }

    public void setThrowableProperties(float thrustForce, float torqueSpin, int _damage, Vector2 direction)
    {
        direction = direction.normalized;
        Debug.Log(direction.x);

        damage = _damage;
        //rb2D.AddForce(transform.right * thrustForce);
        rb2D.AddForce(transform.right * thrustForce, ForceMode2D.Impulse);
        rb2D.AddTorque(direction.x * torqueSpin, ForceMode2D.Force);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        this.transform.parent = collision.transform.parent;
        rb2D.bodyType = RigidbodyType2D.Static;
    }
}
