﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombProjectile : MonoBehaviour
{
    public GameObject explosionParticles;
    public float explosionForce;
    public float radius = 2;
    public int damage;
    public Rigidbody2D rb2D;
    private Transform objectCollided;
    private AudioSource sound;

    public bool firstCollision = false;

    void Awake()
    {
        rb2D = this.gameObject.GetComponent<Rigidbody2D>();
        sound = GetComponent<AudioSource>();
    }

    public void setProjectileProperties(float thrustForce, int _damage)
    {
        damage = _damage;
        rb2D.AddForce(transform.right * thrustForce);
    }

    void Explosion()
    {
        IDestructible destructible = objectCollided.GetComponent<IDestructible>();

        this.transform.parent = null;
        this.GetComponent<MeshRenderer>().enabled = false;


        if (destructible != null)
        {
            destructible.ReplacePrefab();
            
        }

        ApplyForce();

        Instantiate(explosionParticles, this.transform.position, this.transform.rotation);
        sound.Play();

        Destroy(this.gameObject, 0.4f);
    }

    void ApplyForce()
    {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(explosionForce, explosionPos, radius);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(!firstCollision)
        {
            firstCollision = true;

            objectCollided = collision.gameObject.transform;

            this.transform.parent = collision.transform;
            rb2D.bodyType = RigidbodyType2D.Static;

            Invoke("Explosion", 1.5f);
        }
        
    }
}
