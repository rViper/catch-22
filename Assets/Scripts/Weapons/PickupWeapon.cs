﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupWeapon : MonoBehaviour
{

    private GameObject player;
    public bool weaponReady = false;


    void Update()
    {
        // Interactuable (Weapons | Covers)
        if (Input.GetKeyDown(KeyCode.E) && weaponReady)
        {
            WeaponPick();
        }
    }

    void WeaponPick()
    {
        // For the moment it's out since we cant pick up weapons anymore.
        //player.GetComponent<WeaponController>().EquipWeapon(this.gameObject, player.GetComponent<Player>().weaponEquipped);
    }

    void OnTriggerEnter2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            player = entity.transform.gameObject;
            weaponReady = true;
        }
    }

    void OnTriggerExit2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            weaponReady = false;
        }
    }
}
