﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{

    Vector2 speed;
    int damage;
    float parentScale;
    Vector2 startingPosition;

    public Vector2 lastPosition;
    public LayerMask collisionMask;
    public Rigidbody2D rb2D;

    bool stop = false;

    void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        startingPosition = new Vector2(this.transform.position.x, this.transform.position.y);
        lastPosition = this.transform.position;

    }

    void FixedUpdate()
    {
        //rb2D.MovePosition(rb2D.position + speed * Time.deltaTime);
    }

    private void CheckCollisions()
    {

        RaycastHit2D hit = Physics2D.Raycast(lastPosition, this.transform.position, Vector2.Distance(lastPosition, this.transform.position));
        Ray2D myRay = new Ray2D(lastPosition, this.transform.position);

        //Debug.DrawLine(lastPosition, this.transform.position, Color.red);

        if (hit.collider != null && hit.transform.tag == "Player")
        {
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.transform.position = new Vector3(hit.point.x, this.transform.position.y, this.transform.position.z);
            OnHitObject(hit);
            stop = true;
        }
        else if (hit.collider != null && hit.transform.tag == "Environment")
        {
            Destroy(this.gameObject);
        }


        /*
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;


        if (Physics.Raycast(ray, out hit, moveDistance, collisionMask, QueryTriggerInteraction.Collide))
        {
            OnHitObject(hit);
        }*/

        /*
        RaycastHit2D hit = Physics2D.Raycast(lastPosition, this.transform.position, Vector2.Distance(lastPosition, this.transform.position));

        Debug.DrawLine(lastPosition, this.transform.position, Color.red);
        */

    }

    void OnHitObject(RaycastHit2D hit)
    {
        IDamagable damagableObjet = hit.collider.GetComponentInParent<IDamagable>();

        if (damagableObjet != null)
        {
            Vector2 direction = new Vector2(this.transform.position.x - startingPosition.x, this.transform.position.y - startingPosition.y).normalized;

            damagableObjet.TakeDamage(damage, direction, hit.collider.gameObject);
            Destroy(this.gameObject);
        }


        Debug.Log("Colision");
        //GameObject.Destroy(gameObject);
    }

    public void setProjectileProperties(float _speed, int _damage)
    {
        speed = new Vector2(_speed, 0);
        damage = _damage;

        rb2D.AddForce(transform.right * _speed);
    }

    public void setParentScale(float scale)
    {
        parentScale = scale;

        // Sprite direction
        /*if (parentScale == 1)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }*/
    }



    void OnCollisionEnter2D(Collision2D collision)
    {
        IDamagable damagableObjet = collision.gameObject.GetComponentInParent<IDamagable>();

        if (damagableObjet != null)
        {
            Debug.Log("Enemy");

            Vector2 direction = new Vector2(this.transform.position.x - startingPosition.x, this.transform.position.y - startingPosition.y).normalized;

            if (collision.gameObject.tag.Equals("Player"))
            {
                damagableObjet.TakeDamage(damage, direction, collision.gameObject);
            }
            
            Destroy(this.gameObject);
        }

        Destroy(this.gameObject);
    }

}
