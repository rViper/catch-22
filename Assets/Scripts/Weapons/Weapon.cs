﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;

public class Weapon : MonoBehaviour
{
    public int damage;
    public GameObject droppableSpawnable;       // Prefab of the droppable weapon or the weapon is going to get instanced once u pick the dropped one.

    public GameObject DropWeapon()
    {
        GameObject weaponDropped = Instantiate(droppableSpawnable, this.transform.position, Quaternion.identity) as GameObject;
        SetNewAmmo(weaponDropped);

        return weaponDropped;
    }

    protected virtual void SetNewAmmo(GameObject droppedGun) { }
}

public class Gun : Weapon, IShoot
{
    public AudioSource shotSound;
    public AudioSource emptySound;
    public float radiusSound;
    public Transform launchPosition;
    public int ammo;
    public float msBetweenShots;
    public float projectileSpeed;
    public string projectileToSpawn;

    [Space]
    public bool enemyWeapon;

    protected float nextShot;

    public void Shoot(ShootingSoundManager soundManager)
    {
        if (Time.time >= nextShot && ammo != 0)
        {
            //shotSound.Play();
            SoundManager.Instance.PlayOneShotSound("event:/FX/Weapons/Pistol/Empty", transform.position);
            ammo--;
            nextShot = Time.time + msBetweenShots / 1000;

            /******** TEMPORARY SHOT SOUND ***********/
            soundManager.AlertAgents(radiusSound);

            SetProjectile();
        }
        else if (ammo == 0)
        {
            //emptySound.Play();
            SoundManager.Instance.PlayOneShotSound("event:/FX/Weapons/Pistol/Empty", transform.position);
        }
    }

    public void Shoot()
    {
        if (Time.time >= nextShot && ammo != 0)
        {
            //shotSound.Play();
            SoundManager.Instance.PlayOneShotSound("event:/FX/Weapons/Pistol/Shoot", transform.position);
            ammo--;
            nextShot = Time.time + msBetweenShots / 1000;

            SetProjectile();
        }
        else if (ammo == 0)
        {
            //emptySound.Play();
            SoundManager.Instance.PlayOneShotSound("event:/FX/Weapons/Pistol/Empty", transform.position);
        }
    }

    public virtual void SetProjectile()
    {
        if (!enemyWeapon)
        {
            Projectile newProjectile = Instantiate(Resources.Load(projectileToSpawn, typeof(Projectile)), launchPosition.transform.position, launchPosition.transform.rotation) as Projectile;
            // newProjectile.setParentScale(transform.root.localScale.x);
            newProjectile.setProjectileProperties(projectileSpeed, damage);
        }
        else if(enemyWeapon)
        {
            EnemyProjectile newProjectile = Instantiate(Resources.Load(projectileToSpawn, typeof(EnemyProjectile)), launchPosition.transform.position, launchPosition.transform.rotation) as EnemyProjectile;
            // newProjectile.setParentScale(transform.root.localScale.x);
            newProjectile.setProjectileProperties(projectileSpeed, damage);
        }


    }

    protected override void SetNewAmmo(GameObject _droppedGun)
    {
        _droppedGun.GetComponent<Gun>().ammo = this.ammo;
    }
}

public class Firearm : Gun
{
}

public class Shotgun : Gun
{
    public int amountOfBullets;
}

public class C4Launcher : Gun
{
    //public string projectileToSpawn;

    /* EVERY WEAPON IS SHOOTING A PROJECTILE ISNTEAD OF A RAYCAST */
    // The C4Launcher unlike the rest of the weapons, uses a projectile (the C4) instead of a Raycast to shoot, thats why this kind of weapon need it's own Shoot() function.

    public override void SetProjectile()
    {
        BombProjectile newProjectile = Instantiate(Resources.Load(projectileToSpawn, typeof(BombProjectile)), launchPosition.transform.position, launchPosition.transform.rotation) as BombProjectile;
        // newProjectile.setParentScale(transform.root.localScale.x);
        newProjectile.setProjectileProperties(projectileSpeed, damage);
    }

}

public class Throwable : Weapon, IThrow, IMelee
{
    public Transform throwingPosition;
    public float throwForce;
    public string animationName;
    public float animationLength;
    public float torqueSpinForce;

    public string Melee()
    {
        // 1. ACTIVATE DAMAGE TRIGGERS

        // 2. StartCoroutine which will wait "animationLength" and will disable the damage triggers

        return animationName;
    }

    public void Throw()
    {
        // Vector2 direction = new Vector2(playerPosition.x - this.transform.position.x, playerPosition.y - this.transform.position.y).normalized;

        GameObject weaponThrowed = Instantiate(droppableSpawnable, throwingPosition.position, throwingPosition.rotation) as GameObject;
        weaponThrowed.GetComponent<MeleeThrow>().setThrowableProperties(throwForce, torqueSpinForce, damage);
    }
}

public class Hammer : Throwable
{

}







// RAYCAST SHOT
// Inside Shoot() function

/*
        if (Time.time > nextShot && ammo != 0)
        {
            shotSound.Play();
            ammo--;
            nextShot = Time.time + msBetweenShots / 1000;

            Vector3 mousePosition = Input.mousePosition;
            Vector3 difference = Camera.main.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, transform.position.z - Camera.main.transform.position.z));
            Vector3 launchPoint = new Vector3(launchPosition.position.x, launchPosition.position.y, launchPosition.position.z);

            RaycastHit hit;
            Ray myRay = new Ray(launchPoint, difference - launchPoint);

            Physics.Raycast(myRay, out hit);

            Effect();

            Debug.DrawLine(launchPoint, difference, Color.red);

            if (hit.collider.tag == "enemy")
            {
                IDamagable damagableObjet = hit.transform.GetComponentInParent<IDamagable>();

                if (damagableObjet != null)
                {

                }
            }
            
        }
        else if (ammo == 0)
        {
            emptySound.Play();
        }
        */
