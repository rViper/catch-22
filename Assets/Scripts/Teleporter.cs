﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
	public GameObject secondTeletransporter;
	private Transform teleportingPoint;

	private Transform playerPosition;
	public bool teleporterReady = false;


	void Start()
	{
		teleportingPoint = gameObject.transform.Find("TeleporterPosition");
	}

	void Update()
	{
		// Interactuable (Weapons | Covers)
		if (Input.GetKeyDown(KeyCode.E) && teleporterReady)
		{
			StartTeletransport();
		}
	}

	void StartTeletransport()
	{
		Vector3 endingPosition = secondTeletransporter.GetComponent<Teleporter>().teleportingPoint.position;
		playerPosition.position = new Vector3(endingPosition.x, endingPosition.y, playerPosition.position.z);
	}

	void OnTriggerEnter2D(Collider2D entity)
	{
		if (entity.tag == "Player")
		{
			playerPosition = entity.transform;
			teleporterReady = true;
		}
	}

	void OnTriggerExit2D(Collider2D entity)
	{
		if (entity.tag == "Player")
		{
			teleporterReady = false;
		}
	}
}