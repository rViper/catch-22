﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum BlinkingMode
{
    ON,
    OFF,
}

public class PressToStart : MonoBehaviour
{
    public float blinkDelay = 0.7f;
    float timer = 99999;

    Text pressAnyKey;
    BlinkingMode blinkingText = BlinkingMode.ON;

	void Start ()
    {
        pressAnyKey = this.GetComponent<Text>();
	}
	
	void Update()
    {
        if (timer > blinkDelay)
        {
            timer = 0;

            switch (blinkingText)
            {
                case BlinkingMode.ON:
                    pressAnyKey.color = new Color(pressAnyKey.color.r, pressAnyKey.color.g, pressAnyKey.color.b, 1);
                    blinkingText = BlinkingMode.OFF;

                    break;

                case BlinkingMode.OFF:
                    pressAnyKey.color = new Color(pressAnyKey.color.r, pressAnyKey.color.g, pressAnyKey.color.b, 0);
                    blinkingText = BlinkingMode.ON;

                    break;
            }
        }

        timer += Time.deltaTime;
            
    }
}
