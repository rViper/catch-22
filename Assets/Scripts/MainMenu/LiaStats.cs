﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LiaStats : MonoBehaviour {

    public Text temperature;
    public Text bpm;


    float temperatureTimer = 9999;
    float temperatureDelay = 2.3f;

	void Start ()
    {
        temperature = GameObject.Find("TabletCanvas/LiaStats/Temperature").GetComponent<Text>();
        bpm = GameObject.Find("TabletCanvas/LiaStats/BPM").GetComponent<Text>();
    }

    void Update()
    {
        if (temperatureTimer > temperatureDelay)
        {
            CheckTemperature();
            temperatureTimer = 0;
        }

        if (bpm.color.a < 0.2f)
        {
            CheckBPM();
            bpm.color = new Color(bpm.color.r, bpm.color.g, bpm.color.b, 1);
        }


        bpm.color = new Color(bpm.color.r, bpm.color.g, bpm.color.b, bpm.color.a - 0.0075f);

        temperatureTimer += Time.deltaTime;

    }

    void CheckTemperature()
    {
        temperature.text = Random.Range(36.4f, 36.7f).ToString("f1") + "°";
    }

    void CheckBPM()
    {
        bpm.text = Random.Range(70f, 82f).ToString("0") + " BPM";
    }
}
