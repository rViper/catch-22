﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMainMenu : MonoBehaviour {

    float cameraSpeed;

    Transform mainCamera;
    Vector3 targetPosition;
    Quaternion targetRotation;

    void Start()
    {
        mainCamera = Camera.main.transform;
        targetPosition = mainCamera.position;
    }

    void Update()
    {
        mainCamera.transform.position = Vector3.Lerp(mainCamera.position, targetPosition, cameraSpeed * Time.deltaTime);
        mainCamera.transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, cameraSpeed * Time.deltaTime);
    }

    public void SetNewPosition(Vector3 _newPosition, Vector3 _newRotation, float _cameraSpeed)
    {
        targetPosition = _newPosition;
        targetRotation = Quaternion.Euler(_newRotation.x, _newRotation.y, _newRotation.z);

        cameraSpeed = _cameraSpeed;
    }
}
