﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMainMenu : MonoBehaviour
{
    Animator myAnimator;

    void Start()
    {
        myAnimator = this.transform.GetComponent<Animator>();
    }

    public void LaunchOptions()
    {
        myAnimator.Play("ShowOptions");
    }

    public void HideOptions()
    {
        myAnimator.Play("HideOptions");
    }
}
