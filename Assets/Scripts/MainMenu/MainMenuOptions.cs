﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;

enum MenuState
{
    PRESSTART,
    SELECTOPTIONS,
    NEWGAME
}

public class MainMenuOptions : MonoBehaviour
{
    MenuState actualState = MenuState.PRESSTART;

    AudioSource backgroundTheme;
    CameraMainMenu mainCamera;
    GameObject PressAnyButton;
    PostProcessingProfile postProfile;

    GameObject fadeIn;

    public float cameraSpeed;
    public int levelToLoad = 1;

    void Awake()
    {
        fadeIn = GameObject.Find("FadeIn");
        fadeIn.SetActive(false);
    }

	void Start ()
    {
        backgroundTheme = this.GetComponent<AudioSource>();
        mainCamera = Camera.main.GetComponent<CameraMainMenu>();
        PressAnyButton = GameObject.Find("PressAnyButtonCanvas");
        postProfile = mainCamera.GetComponent<PostProcessingBehaviour>().profile;

        postProfile.depthOfField.enabled = false;
    }

	void Update ()
    {
        if (Input.anyKey && actualState == MenuState.PRESSTART)
        {
            GameObject FadeOut = GameObject.Find("FadeOut");
            FadeOut.SetActive(false);

            PressAnyButton.SetActive(false);

            //Vector3 newPosition = new Vector3(0.253f, 2.93f, -6.765f);
            //Vector3 newRotation = new Vector3(48.521f, -175.33f, 3.497f);

            Vector3 newPosition = new Vector3(0.2530001f, 2.905f, -7.027f);
            Vector3 newRotation = new Vector3(62.875f, -173.207f, 6.046f);

            mainCamera.SetNewPosition(newPosition, newRotation, cameraSpeed);
            postProfile.depthOfField.enabled = true;

            actualState = MenuState.SELECTOPTIONS;
        }

        if (actualState == MenuState.SELECTOPTIONS && backgroundTheme.volume < 0.40)
        {
            backgroundTheme.volume += 0.005f;
        }

    }

    void NewGame()
    {
        actualState = MenuState.NEWGAME;

        // Little delay | Game feeling
        Invoke("BlackScreen", 0.1f);
    }

    void LoadNewGame()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void Continue()
    {
        SceneManager.LoadScene(levelToLoad, LoadSceneMode.Single);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    void BlackScreen()
    {
        fadeIn.SetActive(true);
        
        switch (actualState)
        {
            case MenuState.NEWGAME:
                Invoke("LoadNewGame", 2f);
                break;
        }
    }
}
