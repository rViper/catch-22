﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyWall : MonoBehaviour
{
    public Material normalMat;
    public Material jellyMat;
    public Transform gadgetPosition;

    public void BackToNormal()
    {
        this.GetComponent<MeshRenderer>().material = normalMat;
        this.GetComponent<BoxCollider>().enabled = true;
        transform.Find("2DCollider").GetComponent<BoxCollider2D>().enabled = true;
    }

    public void TurnJelly()
    {
        this.GetComponent<MeshRenderer>().material = jellyMat;
        this.GetComponent<BoxCollider>().enabled = false;
        transform.Find("2DCollider").GetComponent<BoxCollider2D>().enabled = false;
    }
}
