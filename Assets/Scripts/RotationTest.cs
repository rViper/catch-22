﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTest : MonoBehaviour
{
    Vector3 mousePosition;

    public Transform point3;

    public bool xVariable = true;
    public bool yVariable = true;
    public bool zVariable = true;

    public float speed = 5f;
    public Transform target;

    void Update ()
    {
        

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            mousePosition = hit.point;
        }



        Vector2 direction = mousePosition - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed * Time.deltaTime);

        //if (Input.GetMouseButton(1))
        //{

        /*
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawLine(ray.origin, ray.direction * 100, Color.yellow);

        Vector3 point = ray.origin + (ray.direction * 100);

        if (!xVariable) point.x = 0;
        if (!yVariable) point.y = 0;
        if (!zVariable) point.z = 0;

        Vector3 aimingPosition = new Vector3(point.x, point.y, point.z);

        //transform.LookAt(aimingPosition);
        //transform.rotation = Quaternion.Euler(0f, 0f, rotationOffeset + rotZ);
        transform.rotation = Quaternion.LookRotation(aimingPosition);
        */

        /*
        Vector3 orbVector = Camera.main.WorldToScreenPoint(Input.mousePosition);
        orbVector = Input.mousePosition - orbVector;
        float angle = Mathf.Atan2(orbVector.y, orbVector.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        */
        //}
    }
}
