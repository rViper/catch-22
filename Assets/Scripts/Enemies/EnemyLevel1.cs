﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLevel1 : Enemy
{
    public float movementSpeed = 7.0f;
    public Animator anim;

    public bool facingRight = true;

    private Vector3 actualWaypointPosition;
    private float moveTimeTotal;
    private float moveTimeCurrent;

 
    [Space]
    [Space]
    [Space]
    public float patrolSize;
    public float patrolSpeed;
    private Vector3 initialPosition;
    public bool right;
    public float sight;
    private bool aimPlayer = false;

    [Space]
    [Space]
    [Space]
    public float timeBetweenShots = 0.33333f;
    public float timestamp;

    [Space]
    public bool movement = true;

    protected override void Start()
    {
        right = true;
        initialPosition = this.transform.position;
        base.Start();

        //PreRagDoll();
    }

    void FixedUpdate()
    {


        RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.right);

        Debug.DrawLine(transform.position, fwd * sight, Color.yellow);


        if (Physics.Raycast(transform.position, fwd, out hit, sight))
        {
            if (hit.collider.tag == "Player")
            {

                Stop();
                
                anim.SetBool("Walking", false);
                anim.SetBool("Aiming", true);

                ShotThatBastard();
            }

            if (hit.collider.tag != "Player" && hit.collider.tag != "Enemy")
            {

                aimPlayer = false;

                anim.SetBool("Walking", false);
                anim.SetBool("Aiming", false);
            }
        }
    }

    void Update()
    {
        Patrol();
        anim.SetBool("Walking", true);
        /*if (movement)
        {
            if (followingPath != null && followingPath.Count > 0 && !aimPlayer)
            {
                anim.SetBool("Walking", true);

                if (transform.position.x - followingPath.Peek().x > 0 && !facingRight) Flip();
                else if (transform.position.x - followingPath.Peek().x < 0 && facingRight) Flip();

                Movement();
            }
        }*/
    }

    private void ShotThatBastard()
    {
        if (Time.time >= timestamp)
        {
            equippedGun.GetComponent<IShoot>().Shoot();
            timestamp = Time.time + timeBetweenShots;
        }
    }

    public void Stop()
    {
        followingPath = null;
        moveTimeTotal = 0;
        moveTimeCurrent = 0;

        anim.SetBool("Walking", false);
        anim.SetBool("Aiming", false);
    }

    private void Movement()
    {
        if (moveTimeCurrent < moveTimeTotal)
        {
            moveTimeCurrent += Time.deltaTime;

            transform.position = Vector3.Lerp(actualWaypointPosition, followingPath.Peek(), moveTimeCurrent / moveTimeTotal);
        }

        // else will mean the agent reached the point.
        else
        {
            actualWaypointPosition = followingPath.Pop();       // Since the agent reached the point, the actualPosition wil be the first waypoint in the stack, so we can delete it.

            // Check if the agent is in a teleporter waypoint
            if (Pathfinding.FindClosestNode(this.transform.position).teleportPoint && followingPath.Count > 0)
            {
                // And check if the next waypoint is a teleporter, which means he wants to use it to follow the path.
                if (Pathfinding.FindClosestNode(followingPath.Peek()).teleportPoint)
                {
                    // If thats true then we instant teleportthe agnet to the next node position and pop it from the queue.
                    actualWaypointPosition = followingPath.Pop();

                    // ****** ADD HERE SOME DELAY AND PARTICLE EFFECTS FOR THE TELEPORT *******

                    transform.position = actualWaypointPosition;
                }
            }

            // if there are no more waypoints on the queue.
            if (followingPath.Count == 0)
                Stop();

            // if there are just calculate the new time it will take to go to that point.
            else
            {
                moveTimeCurrent = 0;
                moveTimeTotal = (actualWaypointPosition - followingPath.Peek()).magnitude / movementSpeed;
            }
        }
    }
    private void Patrol()
    {
        Vector3 initialPositionDistance;
        if (right)
        {
            this.transform.position = new Vector3(this.transform.position.x + patrolSpeed, this.transform.position.y, this.transform.position.z);
            initialPositionDistance = (this.transform.position - initialPosition);
            if (initialPosition.magnitude > patrolSize)
            {
                Flip();
                right = false;
            }
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x-patrolSpeed,this.transform.position.y,this.transform.position.z);
            initialPositionDistance = (this.transform.position - initialPosition);
            if (initialPosition.magnitude < -patrolSize)
            {
                Flip();
                right = true;
            }
        }
    }
    public void Flip()
    {
        /*
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;
        // Multiply the player's x local scale by -1.
        Vector2 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        */


        // Transform the Y rotation depending on which direction is the player going.
        Quaternion localRotation = transform.localRotation;

        if (facingRight) localRotation.y = 180;
        if (!facingRight) localRotation.y = 0;

        transform.localRotation = localRotation;

        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

    }

    public override void Die(Vector2 killDirection, GameObject bodypart)
    {
        base.Die(killDirection, bodypart);

        //RagDoll(killDirection, bodypart);
    }

    public override void SoundAlert(Vector3 sourcePosition)
    {
        base.SoundAlert(sourcePosition);
    }
}
