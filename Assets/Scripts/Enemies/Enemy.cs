﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Entity
{
    public Transform weaponHold;
    public GameObject equippedGun;
    public GameObject ragdoll;

    protected Stack<Vector3> followingPath;

    protected override void Start()
    {
        base.Start();

        GameObject instantiedWeapon = Instantiate(equippedGun, weaponHold.position, weaponHold.rotation) as GameObject;
        instantiedWeapon.transform.parent = weaponHold;
        equippedGun = instantiedWeapon;
    }

    public override void Die(Vector2 direction, GameObject bodyPart)
    {
        base.Die(direction, bodyPart);

        DropWeapon(direction * -1);
        SpawnRagdoll(direction * -1);
    }

    public void NewObjectivePosition(Vector3 targetPosition)
    {
        followingPath = Pathfinding.NavigateTo(targetPosition, this.transform.position);
    }

    public virtual void SoundAlert(Vector3 sourcePosition)
    {
        NewObjectivePosition(sourcePosition);
    }

    private void DropWeapon(Vector2 direction)
    {
        GameObject droppedWeapon = equippedGun.GetComponent<Weapon>().DropWeapon();
        Destroy(equippedGun.gameObject);

        droppedWeapon.GetComponent<Rigidbody2D>().AddForce(new Vector2(direction.x, Vector2.up.y * 2.5f) * Random.Range(30.0f, 50.0f));
        droppedWeapon.GetComponent<Rigidbody2D>().AddTorque(Random.Range(-5.0f, 5.0f), ForceMode2D.Force);
    }

    private void SpawnRagdoll(Vector2 direction)
    {
        GameObject instantiatedRagdoll = Instantiate(ragdoll, this.transform.position, this.transform.rotation) as GameObject;

        instantiatedRagdoll.GetComponentInChildren<Rigidbody>().AddForce(new Vector3(-direction.x, 0, 0) * 300);
    }
}
