﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAimRotator : MonoBehaviour
{
    public Transform shoulder;
    public Transform target;

    public float offsetValue;

    void LateUpdate()
    {
        if (target != null)
        {
            Vector3 difference = target.position - shoulder.position;
            float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            shoulder.rotation = Quaternion.Euler(shoulder.rotation.x, shoulder.rotation.y, rotationZ + offsetValue);
        } 
    }

    public void EnemyDetected(Transform _target)
    {
        target = _target;
    }

    public void EnemyLost()
    {
        target = null;
    }
}
