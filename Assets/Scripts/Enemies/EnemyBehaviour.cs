﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum State
{
    PATROL,
    PATROL_WAIT,
    STATIC,
    LOOK,
    ALERT,
    KILLER,
    SIGHT_CHECK,
    SIGHT_LOST,
    RETURNING           // Once the player has lsot the player, he returns to his original position
}

public class EnemyBehaviour : Enemy
{
    [Space]
    [SerializeField] private State myState;

    
    [Space]
    [Header("Enemy behaviour | Select one of them")]
    public bool patrolMode;
    public bool staticMode;
    public bool lookMode;

    [Space]
    public bool startingRightRotation = true;

    // If patrol mode activated | Point A to point B
    [Space]
    [Header("Patrol Mode | From A to B")]
    private Vector3 originalPosition;
    public Transform patrolA;
    public Transform patrolB;
    private bool toPointA = true;

    [Space]
    public Transform lastSeenTarget;
    public Transform target;

    private float patrolWaitTimer = 0f;     // Timer to increase and check if the enemy has waited enough time
    private float patrolWaitTime = 2.5f;      // Time the enemy has to wait
    private float lookWaitTimer = 0f;       
    private float lookWaitTime = 6f;
    private float sightLostTimer;
    private float sightLostTime = 2;
    private int flipChecksCounter;
    private int flipChecks = 3;

    private Vector3 actualWaypointPosition;
    private float moveTimeTotal;
    private float moveTimeCurrent;
    private float movementSpeed = 2f;

    private float shootTimer;
    private float shootTime = 1.2f;

    private Animator myAnimator;
    private EnemyAimRotator aimRotator;
    private bool facingRight = true;


    void Awake()
    {
        SetBehaviour();
    }

    // Use this for initialization
    protected override void Start ()
    {
        originalPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
        myAnimator = this.transform.Find("Mesh").GetComponent<Animator>();
        aimRotator = this.GetComponent<EnemyAimRotator>();

        base.Start();
	}
	
	void Update ()
    {
        CheckFieldStatus();

        if (myState == State.PATROL)
        {
            if (followingPath != null && followingPath.Count > 0)
            {
                myAnimator.SetBool("Walking", true);

                Movement();
            }
        }

        else if (myState == State.PATROL_WAIT)
        {
            patrolWaitTimer += Time.deltaTime;

            if (patrolWaitTimer > patrolWaitTime)
            {
                if (toPointA)
                {
                    NewObjectivePosition(patrolA.position);
                    toPointA = false;
                }
                else
                {
                    NewObjectivePosition(patrolB.position);
                    toPointA = true;
                }

                myState = State.PATROL;
            }
        }

        else if (myState == State.STATIC)
        {

        }

        else if (myState == State.LOOK)
        {
            lookWaitTimer += Time.deltaTime;

            if (lookWaitTimer > lookWaitTime)
            {
                Flip();
                lookWaitTimer = 0;
            }

        }

        else if (myState == State.KILLER)
        {
            shootTimer += Time.deltaTime;

            if (shootTimer > shootTime)
            {
                equippedGun.GetComponent<IShoot>().Shoot();
                shootTimer = 0;
            }
        }

        else if (myState == State.SIGHT_CHECK)
        {
            if (followingPath != null && followingPath.Count > 0)
            {
                myAnimator.SetBool("Walking", true);

                Movement();
            }
            else
            {
                myState = State.SIGHT_LOST;
            }
        }

        else if (myState == State.SIGHT_LOST)
        {
            sightLostTimer += Time.deltaTime;

            if (sightLostTimer > sightLostTime)
            {
                Flip();
                sightLostTimer = 0;
                flipChecksCounter++;
            }

            if (flipChecksCounter > 3)
            {
                NewObjectivePosition(originalPosition);
                myState = State.RETURNING;
            }               
        }

        else if (myState == State.RETURNING)
        {
            if (followingPath != null && followingPath.Count > 0)
            {
                myAnimator.SetBool("Walking", true);

                Movement();
            }
            else
            {
                SetBehaviour();
            }    
        }
    }

    private void SetBehaviour()
    {
        if (patrolMode)
        {
            NewObjectivePosition(patrolB.position);
            toPointA = true;

            myState = State.PATROL;
        }

        else if (staticMode)
            myState = State.STATIC;
        else
            myState = State.LOOK;

        Flip(startingRightRotation);
    }

    // Check if theres an enemy in sight
    private void CheckFieldStatus()
    {
        if (target != null && myState != State.KILLER)
        {
            Stop();
            myState = State.KILLER;
            aimRotator.EnemyDetected(target);
        }

        // This means the enemy lost the target.
        else if (target == null && myState == State.KILLER)
        {
            NewObjectivePosition(lastSeenTarget.position);
            myState = State.SIGHT_CHECK;
            aimRotator.EnemyLost();
        }
    }

    private void Movement()
    {
        if (moveTimeCurrent < moveTimeTotal)
        {
            moveTimeCurrent += Time.deltaTime;

            transform.position = Vector3.Lerp(actualWaypointPosition, followingPath.Peek(), moveTimeCurrent / moveTimeTotal);
        }

        // else will mean the agent reached the point.
        else
        {
            actualWaypointPosition = followingPath.Pop();       // Since the agent reached the point, the actualPosition wil be the first waypoint in the stack, so we can delete it.

            // Check if the agent is in a teleporter waypoint
            if (Pathfinding.FindClosestNode(this.transform.position).teleportPoint && followingPath.Count > 0)
            {
                // And check if the next waypoint is a teleporter, which means he wants to use it to follow the path.
                if (Pathfinding.FindClosestNode(followingPath.Peek()).teleportPoint)
                {
                    // If thats true then we instant teleportthe agnet to the next node position and pop it from the queue.
                    actualWaypointPosition = followingPath.Pop();

                    // ****** ADD HERE SOME DELAY AND PARTICLE EFFECTS FOR THE TELEPORT *******

                    transform.position = actualWaypointPosition;
                }
            }

            // if there are no more waypoints on the queue.
            if (followingPath.Count == 0)
            {
                Stop();
                if (myState == State.PATROL) myState = State.PATROL_WAIT;
                else if (myState == State.SIGHT_CHECK) myState = State.SIGHT_LOST;
                else if (myState == State.RETURNING) SetBehaviour();
            }
                

            // if there are just calculate the new time it will take to go to that point.
            else
            {
                moveTimeCurrent = 0;
                moveTimeTotal = (actualWaypointPosition - followingPath.Peek()).magnitude / movementSpeed;

                if (transform.position.x - followingPath.Peek().x < 0 && !facingRight) Flip();
                else if (transform.position.x - followingPath.Peek().x > 0 && facingRight) Flip();
            }
        }
    }

    public void Stop()
    {
        followingPath = null;
        moveTimeTotal = 0;
        moveTimeCurrent = 0;

        patrolWaitTimer = 0;
        lookWaitTimer = 0;
        sightLostTimer = 0;
        flipChecksCounter = 0;


        myAnimator.SetBool("Walking", false);
        myAnimator.SetBool("Aiming", false);
    }

    // Rotate player depending on it's direction

    public void Flip(bool startingRight)
    {
        // Transform the Y rotation depending on which direction is the player going.
        Quaternion localRotation = transform.localRotation;

        if (!startingRight) localRotation.y = 180;
        if (startingRight) localRotation.y = 0;

        transform.localRotation = localRotation;

        // Switch the way the player is labelled as facing.
        facingRight = startingRight;
    }

    public void Flip()
    {
        // Transform the Y rotation depending on which direction is the player going.
        Quaternion localRotation = transform.localRotation;

        if (facingRight) localRotation.y = 180;
        if (!facingRight) localRotation.y = 0;

        transform.localRotation = localRotation;

        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;
    }
}
