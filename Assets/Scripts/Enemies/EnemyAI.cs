﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : Enemy
{
   
    public Animator anim;

    public bool facingRight = true;

    private bool wait = false;
    private float waitingTime;
    private float maxWait=3.0f;

    public float patrolSize;
    public float patrolSpeed;
    private Vector3 initialPosition;
    public bool right;


    /******
     * PROVIOSIONAL
     ******/
    [Space]
    public float timeBetweenShots = 0.33333f;
    public float timestamp;

    public bool enemyInSight = false;
    

    protected override void Start()
    {
        right = true;
        initialPosition = this.transform.position;
        base.Start();

        //PreRagDoll();
    }


    void Update()
    {
        if (enemyInSight)
        {
            anim.SetBool("Walking", false);
            anim.SetBool("Aiming", true);

            ShotThatBastard();
        }

        else if (!wait)
        {
            Patrol();
            anim.SetBool("Walking", true);
        }
        else
        {
            Wait();
            anim.SetBool("Walking", false);
        }
    }


    /*******
    * PROVIOSIONAL 
    ********/
    private void ShotThatBastard()
    {
        if (Time.time >= timestamp)
        {
            equippedGun.GetComponent<IShoot>().Shoot();
            timestamp = Time.time + timeBetweenShots;
        }
    }


    private void Patrol()
    {
        if (right)
        {
            this.transform.position = new Vector3(this.transform.position.x + (patrolSpeed * Time.deltaTime), this.transform.position.y, this.transform.position.z);
            if (-initialPosition.x+this.transform.position.x > patrolSize)
            {
                waitingTime = 0;
                wait = true;
                right = false;
            }
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x - (patrolSpeed * Time.deltaTime), this.transform.position.y, this.transform.position.z);
            if (-initialPosition.x + this.transform.position.x < -patrolSize)
            {
                waitingTime = 0;
                wait = true;
                right = true;
            }
        }
    }
    private void Wait()
    {
        waitingTime += Time.deltaTime;
        if (waitingTime > maxWait)
        {
            Flip();
            wait = false;
        }
    }
    public void Flip()
    {
        /*
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;
        // Multiply the player's x local scale by -1.
        Vector2 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        */


        // Transform the Y rotation depending on which direction is the player going.
        Quaternion localRotation = transform.localRotation;

        if (facingRight) localRotation.y = 180;
        if (!facingRight) localRotation.y = 0;

        transform.localRotation = localRotation;

        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

    }

    public override void Die(Vector2 killDirection, GameObject bodypart)
    {
        base.Die(killDirection, bodypart);

        //RagDoll(killDirection, bodypart);
    }

    public override void SoundAlert(Vector3 sourcePosition)
    {
        base.SoundAlert(sourcePosition);
    }
}
