﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class DoorMesh : MonoBehaviour {

    public int doorDamage;
    public float forceThreshold;
    Rigidbody2D rb2D;


    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D entity)
    {
        if (entity.collider.tag == "Enemy" && rb2D.velocity.magnitude > forceThreshold)
        {
            Vector2 direction = new Vector2(entity.transform.position.x - this.transform.position.x, entity.transform.position.y -this.transform.position.y).normalized;

            entity.collider.GetComponent<IDamagable>().TakeDamage(doorDamage, direction, null);

            Debug.Log(rb2D.velocity.magnitude);
            Debug.Log("On the enemy's head!");
        }
    }
}
