﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoverEnvironment : MonoBehaviour
{

    [Space]
    public NewCameraAngle leftCameraTrigger;
    public NewCameraAngle rightCameraTrigger;

    [Space]
    public Transform leftCoverPosition;
    public Transform rightCoverPosition;
    public GameObject coverCollider;

    public bool coverReady = false;
    bool alreadyInUse = false;
    public GameObject entityCovering;

    Transform playerPosition;

    void Update()
    {
        // Interactuable (Weapons | Covers)
        if (Input.GetKeyDown(KeyCode.E) && coverReady && !alreadyInUse)
        {
            //entityCovering = entity.gameObject;
            entityCovering.GetComponent<Player>().movement = false;
            SetupCover();
        }
        else if (Input.GetKeyDown(KeyCode.E) && alreadyInUse)
        {
            entityCovering.GetComponent<Player>().movement = true;
            FinishCover();
        }
    }

    void SetupCover()
    {
        Vector2 direction = new Vector2(playerPosition.transform.position.x - this.transform.position.x, playerPosition.transform.position.y - this.transform.position.y).normalized;

        // Start animation
        alreadyInUse = true;
        coverCollider.GetComponent<BoxCollider2D>().enabled = true;
        coverCollider.transform.localEulerAngles = new Vector3(0, 0, 0);

        if (direction.x < 0)
        {
            entityCovering.transform.position = new Vector3(leftCoverPosition.position.x, entityCovering.transform.position.y, entityCovering.transform.position.z);
            leftCameraTrigger.StartCamera();
        }
        else
        {
            entityCovering.transform.position = new Vector3(rightCoverPosition.position.x, entityCovering.transform.position.y, entityCovering.transform.position.z);
            rightCameraTrigger.StartCamera();
        }
    }

    void FinishCover()
    {
        alreadyInUse = false;
        coverCollider.GetComponent<BoxCollider2D>().enabled = false;
        coverCollider.transform.localEulerAngles = new Vector3(0, 0, 90);

        leftCameraTrigger.FinishCamera();
    }

    void OnTriggerEnter2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            playerPosition = entity.transform;
            coverReady = true;
            entityCovering = entity.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            coverReady = false;
        }
    }
}