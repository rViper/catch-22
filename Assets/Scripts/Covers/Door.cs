﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    Transform playerPosition;
    Transform doorMesh;

    bool doorReady = false;
    bool doorOpened = false;
    bool brokenDoor = false;

    public float forceApplied = 500;

    public AudioSource knockingSound;

    public Vector2 direction;

    void Start()
    {
        doorMesh = this.gameObject.transform.GetChild(0);
    }

    void Update()
    {
        if (!brokenDoor)
        {
            // Interactuable (Weapons | Covers)
            if (Input.GetKeyDown(KeyCode.E) && doorReady)
            {
                openDoor();
            }
            else if (Input.GetKeyDown(KeyCode.R) && doorReady && !doorOpened)
            {
                knockKnock();
            }
        }
    }

    void FixedUpdate()
    {

        if (Input.GetKeyDown(KeyCode.F) && doorReady && !doorOpened && !brokenDoor)
        {
            Rigidbody2D rb2D = doorMesh.GetComponent<Rigidbody2D>();

            SoundManager.Instance.PlayOneShotSound("event:/FX/Door/DoorHit", transform.position);

            rb2D.isKinematic = false;
            rb2D.AddForce(new Vector2(direction.x, 0) * forceApplied);

            brokenDoor = true;

            Invoke("InvalidRigidbody", 2);
        }
    }

    void InvalidRigidbody()
    {
        Rigidbody2D rb2D = doorMesh.GetComponent<Rigidbody2D>();

        rb2D.isKinematic = true;
        doorMesh.GetComponent<Collider2D>().enabled = false;
    }

    void openDoor()
    {
        // Start animation
        if (!doorOpened)
        {
            SoundManager.Instance.PlayOneShotSound("event:/FX/Door/OpenDoor", transform.position);

            doorMesh.GetComponent<BoxCollider2D>().enabled = false;
            doorMesh.transform.localEulerAngles = new Vector3(0, -90, 0);
            doorOpened = true;
        }
        else
        {
            SoundManager.Instance.PlayOneShotSound("event:/FX/Door/CloseDoor", transform.position);

            doorMesh.GetComponent<BoxCollider2D>().enabled = true;
            doorMesh.transform.localEulerAngles = new Vector3(0, 0, 0);
            doorOpened = false;
        }
        
    }

    void knockKnock()
    {
        Vector2 direction = new Vector2(playerPosition.transform.position.x - this.transform.position.x, playerPosition.transform.position.y - this.transform.position.y).normalized;
        float thresHold = 0.8f * direction.x;

        playerPosition.transform.position = new Vector3(doorMesh.position.x + thresHold, playerPosition.position.y, playerPosition.position.z);

        knockingSound.Play();
    }

    void OnTriggerEnter2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            playerPosition = entity.transform;
            doorReady = true;
            direction = new Vector2(doorMesh.transform.position.x - playerPosition.position.x, 0).normalized;
        }
    }

    void OnTriggerExit2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            doorReady = false;
        }
    }
}
