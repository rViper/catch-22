﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GadgetController : MonoBehaviour
{
    public Transform gadgetHold;
    public GameObject[] gadgetBelt = new GameObject[4];         // Array with the different weapons available to the player;
    public int gadgetSelected = 0;

    //WeaponController weaponController;

    private bool gadgetUnsheated = true;

    void Start ()
    {
        // Instantiate all gadgets in the belt so they are available once the player tries to swap between them.
        for (int i = 0; i < gadgetBelt.Length; i++)
        {
            if (gadgetBelt[i] != null)
            {
                GameObject instantiatedGadget = Instantiate(gadgetBelt[i], gadgetHold.position, gadgetHold.rotation) as GameObject;
                instantiatedGadget.transform.parent = gadgetHold;
                gadgetBelt[i] = instantiatedGadget;
                if (i == 0) gadgetBelt[i].SetActive(true);


                if (!instantiatedGadget.GetComponent<GrapplingHook>()) NewHud.hud.SetAmmo(i, instantiatedGadget);
            }
        }
    }
	
    // Swap between the available gadgets
    public void SwapGadget(bool upDown, bool gadgetCondition)
    {
        if (upDown && gadgetSelected < gadgetBelt.Length - 1)
        {
            gadgetBelt[gadgetSelected].SetActive(false);
            gadgetSelected++;
            gadgetBelt[gadgetSelected].SetActive(!gadgetCondition);
        }
        else if(!upDown && gadgetSelected > 0)
        {
            gadgetBelt[gadgetSelected].SetActive(false);
            gadgetSelected--;
            gadgetBelt[gadgetSelected].SetActive(!gadgetCondition);
        }

        NewHud.hud.SelectItem(gadgetSelected);
    }

    public void SpecificGadget(int gadgetPos)
    {
        gadgetBelt[gadgetSelected].SetActive(false);
        gadgetSelected = gadgetPos;
        gadgetBelt[gadgetSelected].SetActive(true);

        NewHud.hud.SelectItem(gadgetSelected);
    }

    // Activate or deactivate the gadget once the player press Q.
    public void ActivateGadget(bool value)
    {
        value = !value;

        gadgetBelt[gadgetSelected].SetActive(value);
        NewHud.hud.HideUnhide(value);
    }

    public void UseGadget(bool facingRight)
    {
        int direction = 0;

        if (facingRight) direction = 1;
        else direction = -1;

        // Hook
        if (gadgetSelected == 0) gadgetBelt[gadgetSelected].GetComponent<GrapplingHook>().LaunchHook();

        // Gadgets
        else
        {
            gadgetBelt[gadgetSelected].GetComponent<IDeploy>().Deploy(direction);
            NewHud.hud.SetAmmo(gadgetSelected, gadgetBelt[gadgetSelected]);
        }   
    }
}
