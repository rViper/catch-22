﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gadgets : MonoBehaviour, IDeploy
{
    public float throwForce = 80;

    public int ammo = 4;
    public GameObject usableItem;
    public bool groundGadget;

    // direction = +1 || -1
    public void Deploy(int direction)
    {
        if (ammo > 0)
        {
            if (groundGadget)
            {
                GameObject newGadget = Instantiate(usableItem, transform.position, Quaternion.identity) as GameObject;

                newGadget.GetComponent<Rigidbody2D>().AddForce((direction * new Vector2(1, 0)) * throwForce);
                ammo--;
            }
            else
            {
                float rotationValue = 0;

                if (direction > 0)
                    rotationValue = 180;

                // Check distance from player and if its < X then put the gadget in the wall in front of him.
                float checkDistance = 0.7f;

                RaycastHit hit;
                Vector3 fwd = Player.player.transform.TransformDirection(Vector3.right);

                Debug.DrawLine(transform.position, fwd * checkDistance, Color.yellow);

                if (Physics.Raycast(transform.position, fwd, out hit, checkDistance))
                {
                    if (hit.collider.tag == "JellyWall")
                    {
                        Transform gadgetPosition = hit.transform.GetComponent<JellyWall>().gadgetPosition;

                        GameObject newGadget = Instantiate(usableItem, gadgetPosition.position, Quaternion.Euler(0, 0, rotationValue)) as GameObject;
                        newGadget.GetComponent<WallDisruptor>().StartAnimation(hit.transform.gameObject, this.transform.gameObject);

                        ammo--;
                    }
                }
            }
        } 
    }

    public void AmmoModify(int newValue)
    {
        ammo += newValue;

        if (ammo > 5)
            ammo = 5;
        else if (ammo < 0)
            ammo = 0;
    }
}
