﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalHook : MonoBehaviour
{
    public GameObject enemyRagdoll;
    public GameObject verticalTrap;

    private Transform hookPoint;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Enemy"))
        {
            GameObject newTrap = Instantiate(verticalTrap, new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z), this.transform.rotation) as GameObject;
            GameObject newRagdoll = Instantiate(enemyRagdoll, newTrap.transform.position, Quaternion.Euler(collision.transform.rotation.x, collision.transform.rotation.y, -180)) as GameObject;

            Destroy(collision.gameObject);

            HingeJoint trapJoint = newTrap.GetComponent<HingeJoint>();

            foreach (Transform t in newRagdoll.transform)
            {
                foreach (Transform grandChild in t)
                {
                    foreach (Transform grandGrandChild in grandChild)
                    {
                        foreach (Transform grandGrandGrandChild in grandGrandChild)
                        {
                            foreach (Transform grandGrandGrandGrandChild in grandGrandGrandChild)
                            {
                                if (grandGrandGrandGrandChild.name == "<HOOK_POINT>")
                                {
                                    hookPoint = grandGrandGrandGrandChild;
                                }
                            }
                        }
                    }
                }  
            }

            trapJoint.connectedBody = hookPoint.GetComponent<Rigidbody>();

            Destroy(this.gameObject);
        }
    }
}
