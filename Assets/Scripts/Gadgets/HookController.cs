﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookController : MonoBehaviour
{
    public Transform hookHold;
    public GameObject hookBelt;       // Array with the different weapons available to the player;

    //WeaponController weaponController;

    private bool gadgetUnsheated = false;

    void Start ()
    {
        //weaponController = GetComponent<WeaponController>();

        if (hookBelt != null)
        {
            GameObject instantiedGadget = Instantiate(hookBelt, hookHold.position, hookHold.rotation) as GameObject;
            instantiedGadget.transform.parent = hookHold;
            hookBelt = instantiedGadget;
        }
        
        //gadgetBelt[0].SetActive(false);

        //if (gadgetBelt[0] != null)
            //GameObject.Find("HUD").GetComponent<HUD>().SetGadget(gadgetBelt[0].G);
    }
	

    public void UseHook()
    {
        //if (gadgetBelt[0] != null)
        //GameObject.Find("HUD").GetComponent<HUD>().SetGadget(gadgetBelt[0]);

        hookBelt.GetComponent<GrapplingHook>().LaunchHook();
    }

    public void ActivateHook(bool value)
    {
        hookBelt.SetActive(value);
    }
}
