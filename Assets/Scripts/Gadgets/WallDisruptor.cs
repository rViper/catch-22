﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDisruptor : MonoBehaviour
{
    private GameObject disruptedWall;   // The wall the player was looking for the moment this item got instantiated;
    private GameObject originalGadget;  // The gadget used to instantiate this item;

    private bool inRange = false;

    void Update()
    {
        if (inRange)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                disruptedWall.GetComponent<JellyWall>().BackToNormal();
                originalGadget.GetComponent<Gadgets>().AmmoModify(+1);
                Destroy(this.gameObject);
            }
        }
    }

    public void StartAnimation(GameObject _disruptedWall, GameObject _originalGadget)
    {
        disruptedWall = _disruptedWall;
        originalGadget = _originalGadget;

        //Start Animation and then we will call the next function.
        ActivateWall();
    }

    public void ActivateWall()
    {
        disruptedWall.GetComponent<JellyWall>().TurnJelly();
    }

    public void DeactivateWall()
    {
        disruptedWall.GetComponent<JellyWall>().BackToNormal();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag.Equals("Player"))
            inRange = true;
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
            inRange = false;
    }
}
