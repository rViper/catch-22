﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalHookBoost : MonoBehaviour
{
    private Vector3 originalPos;
    private Vector3 desiredPos;

    private Vector3 newPosition;

    bool collision = false;

    void Start()
    {
        originalPos = this.transform.position;
        desiredPos = new Vector3(originalPos.x, originalPos.y + 4.5f, originalPos.z);
    }

    void Update()
    {
        if (!collision)
        {
            if (this.transform.position.y < desiredPos.y)
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.05f, this.transform.position.z);
            }
            else
            {
                newPosition = transform.position;

                newPosition.y += (Mathf.Sin(Time.time) * Time.deltaTime) * 0.15f;
                transform.position = newPosition;
            }
        }
    }

    private void OnCollisionEnter(Collision itemCollision)
    {
        collision = true;
    }
}
