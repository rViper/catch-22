﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SmokeGrenade : MonoBehaviour
{
    public GameObject enemyRagdoll;


    private void Start()
    {
        Destroy(this.gameObject, 5.0f);

        SoundManager.Instance.PlayOneShotSound("event:/FX/Gadgets/SmokeGrenade/OpenGas", transform.position);
        SoundManager.Instance.PlayOneShotSound("event:/FX/Gadgets/SmokeGrenade/IdleGas", transform.position);
    }

    private IEnumerator TimeToSleep(GameObject enemy)
    {
        yield return new WaitForSeconds(2.0f);

        GameObject newRagdoll = Instantiate(enemyRagdoll, enemy.transform.position, enemy.transform.rotation) as GameObject;
        Destroy(enemy.gameObject);
    }

        void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag.Equals("Enemy"))
        {
            StartCoroutine(TimeToSleep(collider.gameObject));
        }
    }
}
