﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingHook : MonoBehaviour
{
    public Sprite icon;

    [Space]
    public float launchForce;

    [Space]
    public string spawningHook;
    public Transform launchPosition;

    private bool canShoot = true;


    public void LaunchHook()
    {
        if (canShoot)
        {
            canShoot = false;
            Hook newHook = Instantiate(Resources.Load(spawningHook, typeof(Hook)), launchPosition.transform.position, launchPosition.transform.rotation) as Hook;
            // newProjectile.setParentScale(transform.root.localScale.x);

            SoundManager.Instance.PlayOneShotSound("event:/FX/Hook/ThrowHook", transform.position);

            newHook.SetParameters(this.gameObject, launchPosition, launchForce);
        }
    }

    public void SetCanShoot(bool can) { canShoot = can; }
}
