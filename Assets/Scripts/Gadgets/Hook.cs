﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : MonoBehaviour
{
    private GameObject grapplingHookGun;

    private bool returnToGun;
    private bool launchPlayer;

    public float movingSpeed = 3.0f;
    private float moveTimeTotal;
    private float moveTimeCurrent;

    private bool triggerActivated = false;

    AutoRotate rotation;
    LineRenderer lineRenderer;
    Rigidbody2D myRigidbody2D;

    GameObject player;
    Transform launchPosition;

    

    void Awake()
    {
        myRigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        rotation = GetComponent<AutoRotate>();
        lineRenderer = GetComponent<LineRenderer>();


        player = Player.player.gameObject;

        SoundManager.Instance.PlayOneShotSound("event:/FX/Hook/IdleHook", transform.position);

        //lineRenderer.set
    }

    void Update()
    {
        Vector3[] linPositions = { launchPosition.position, this.transform.position } ;
        lineRenderer.SetPositions(linPositions);

        if (Input.GetKeyDown(KeyCode.Space) && !returnToGun)
            ReturningAction();

        if (returnToGun)
        {
            moveTimeCurrent += Time.deltaTime;

            transform.position = Vector3.Lerp(transform.position, launchPosition.position, moveTimeCurrent / moveTimeTotal);
        }
        else if(launchPlayer)
        {
            moveTimeCurrent += Time.deltaTime;

            Vector3 newPlayerPosition = Vector3.Lerp(player.transform.position, transform.position, moveTimeCurrent / moveTimeTotal);

            player.transform.position = new Vector3(newPlayerPosition.x, newPlayerPosition.y, player.transform.position.z);
        }

    }

    public void SetParameters(GameObject _grapplingHookGun, Transform _launchPosition, float launchForce)
    {
        grapplingHookGun = _grapplingHookGun;
        launchPosition = _launchPosition;
        myRigidbody2D.AddForce(transform.right * launchForce);
    }

    private void ReturningAction()
    {
        returnToGun = true;

        moveTimeCurrent = 0;
        moveTimeTotal = (transform.position - launchPosition.position).magnitude / movingSpeed;

        Destroy(this.gameObject, moveTimeTotal - 0.25f);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Stop rotation script so it stops rotating on collision.
        rotation.enabled = false;
        myRigidbody2D.isKinematic = true;
        myRigidbody2D.velocity = new Vector2(0, 0);

        SoundManager.Instance.PlayOneShotSound("event:/FX/Hook/HookHit", transform.position);

        if (collision.gameObject.layer == LayerMask.NameToLayer("Hookable"))
        {
            launchPlayer = true;

            moveTimeCurrent = 0;
            moveTimeTotal = (transform.position - launchPosition.position).magnitude / movingSpeed;

            Destroy(this.gameObject, moveTimeTotal - 0.25f);
        }
            
        else
        {
            ReturningAction();
        }   

        this.GetComponent<BoxCollider2D>().enabled = false;
        triggerActivated = true;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (triggerActivated)
        {
            if (collision.gameObject.tag.Equals("Player"))
                Destroy(this.gameObject);
        }
    }

    void OnDestroy()
    {
        grapplingHookGun.GetComponent<GrapplingHook>().SetCanShoot(true);
    }
}
