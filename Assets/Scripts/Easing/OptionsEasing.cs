﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class OptionsEasing : MonoBehaviour
{
    enum State
    {
        BOTTOM,
        MID,
        TOP
    };


    [SerializeField] float transitionTime = 0.7f;
    float currentLerpTime;

    // GameObjects to be affected
    [Space]
    [SerializeField] RectTransform purpleShape;
    [SerializeField] RectTransform whiteShape;

    // Desired positions
    [Space]
    [SerializeField] RectTransform midSavedPosition;
    [SerializeField] RectTransform initialSavedPosition;                            // Positions created in the herarchy
    [SerializeField] RectTransform finalSavedPosition;                              // Positions created in the herarchy
    RectTransform[] initialPosition = new RectTransform[2];                         // Initial position set once the Option Menu is called
    RectTransform[] desiredPosition = new RectTransform[2];                         // Desired position that the menu will have once the Option Menu is called

    // Effects for the transition
    [Space]
    [SerializeField] EasingFunction.Ease IN_EASE =  EasingFunction.Ease.EaseOutBounce;  // Function to enter
    [SerializeField] EasingFunction.Ease OUT_EASE = EasingFunction.Ease.EaseInBounce;   // Function to leave the mid screen
    EasingFunction.Ease easingFunction;                                                 // Easing function that will be used at the end

    // Actual state | False = Initial (bottom position) \ True = Final (top position)
    [SerializeField] State lastState = State.BOTTOM;
    [SerializeField] State actualState = State.BOTTOM;

    // Vector that will be used for the easing function
    Vector2[] easingVector = new Vector2[2];    // 0 - Purple | 1 - White

    // Controls window
    Animator myAnimator;
    bool controlsActive = false;
    

	void Start ()
    {
        myAnimator = this.transform.GetComponent<Animator>();

        midSavedPosition = this.transform.Find("Mid Position").GetComponent<RectTransform>();
        initialSavedPosition = this.transform.Find("Initial Position").GetComponent<RectTransform>();
        finalSavedPosition = this.transform.Find("Final Position").GetComponent<RectTransform>();

        purpleShape.localPosition = initialSavedPosition.localPosition;
        whiteShape.localPosition = initialSavedPosition.localPosition;

        currentLerpTime = transitionTime;
    }
	
	void Update ()
    {
		if (currentLerpTime < transitionTime)
        {
            currentLerpTime += Time.deltaTime;
            float t = currentLerpTime / transitionTime;

            // Position
            easingVector[0].x = EasingFunction.GetEasingFunction(easingFunction)(initialPosition[0].localPosition.x, desiredPosition[0].localPosition.x, t);
            easingVector[0].y = EasingFunction.GetEasingFunction(easingFunction)(initialPosition[0].localPosition.y, desiredPosition[0].localPosition.y, t);

            easingVector[1].x = EasingFunction.GetEasingFunction(easingFunction)(initialPosition[1].localPosition.x, desiredPosition[1].localPosition.x, t);
            easingVector[1].y = EasingFunction.GetEasingFunction(easingFunction)(initialPosition[1].localPosition.y, desiredPosition[1].localPosition.y, t);

            // Assing the positions to the shapes
            purpleShape.localPosition = easingVector[0];
            whiteShape.localPosition = easingVector[1];
        }
	}

    public void Launch()
    {
        currentLerpTime = 0;

        if (actualState == State.BOTTOM)
        {
            easingFunction = IN_EASE;
            initialPosition[0] = initialSavedPosition;
            initialPosition[1] = finalSavedPosition;
            desiredPosition[0] = midSavedPosition;
            desiredPosition[1] = midSavedPosition;

            lastState = actualState;
            actualState = State.MID;
        }

        else if (actualState == State.MID)
        {
            easingFunction = OUT_EASE;

            initialPosition[0] = midSavedPosition;
            initialPosition[1] = midSavedPosition;


            if (lastState == State.BOTTOM)
            {
                desiredPosition[0] = finalSavedPosition;
                desiredPosition[1] = initialSavedPosition;
                lastState = actualState;
                actualState = State.TOP;
            }

            else
            {
                desiredPosition[0] = initialSavedPosition;
                desiredPosition[1] = finalSavedPosition;
                lastState = actualState;
                actualState = State.BOTTOM;
            }

            if (controlsActive) LaunchControls();   
        }

        else
        {
            easingFunction = IN_EASE;
            initialPosition[0] = finalSavedPosition;
            initialPosition[1] = initialSavedPosition;
            desiredPosition[0] = midSavedPosition;
            desiredPosition[1] = midSavedPosition;

            lastState = actualState;
            actualState = State.MID;
        }

        
    }

    public void LaunchControls()
    {
        if (!controlsActive)
        {
            myAnimator.Play("InControls");
        }

        else
        {
            myAnimator.Play("OutControls");
        }
            

        controlsActive = !controlsActive;
    }
}
