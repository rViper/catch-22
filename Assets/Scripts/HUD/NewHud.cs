﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewHud : MonoBehaviour
{
    public static NewHud hud;

    private GameObject selector;

    public GameObject[] ammo1 = new GameObject[4];
    public GameObject[] ammo2 = new GameObject[4];
    public GameObject[] ammo3 = new GameObject[4];
    public GameObject[] ammo4 = new GameObject[4];

    private float standardValue = -155;
    private float distanceBetween = 107;

	void Awake ()
    {
        if (hud != null)
            Destroy(this.gameObject);
        else
            hud = this;

        selector = GameObject.Find("Selector");
        selector.SetActive(true);
    }


    public void SetAmmo(int gadgetPos, GameObject gadget)
    {
        int currentAmmo = gadget.GetComponent<Gadgets>().ammo;

        if (gadgetPos == 0)
        {
            for (int i = ammo1.Length; i > currentAmmo; i--)
            {
                
                ammo1[i-1].SetActive(false);
            }
        }
        else if (gadgetPos == 1)
        {
            for (int i = ammo2.Length; i > currentAmmo; i--)
            {
                ammo2[i-1].SetActive(false);
            }
        }
        else if (gadgetPos == 2)
        {
            for (int i = ammo3.Length; i > currentAmmo; i--)
            {
                ammo3[i-1].SetActive(false);
            }
        }
        else if (gadgetPos == 3)
        {
            for (int i = ammo4.Length; i > currentAmmo; i--)
            {
                ammo4[i-1].SetActive(false);
            }
        }
    }

    public void SelectItem(int actualGadget)
    {
        RectTransform selectorTransform = selector.GetComponent<RectTransform>();
        float newValue = standardValue + (distanceBetween * actualGadget);

        Debug.Log(newValue);

        selectorTransform.localPosition = new Vector3(newValue, selectorTransform.localPosition.y, selectorTransform.localPosition.z);
    }

    public void HideUnhide(bool value)
    {
        selector.SetActive(value);
    }
}
