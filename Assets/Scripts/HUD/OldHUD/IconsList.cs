﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconsList : MonoBehaviour
{
    [Serializable]
    private class ItemSpriteEntry
    {
        public String item;
        public Sprite image;
    }

    [SerializeField]
    private List<ItemSpriteEntry> itemList;

    private Dictionary<String, Sprite> itemSpriteDictionary;

    private void Awake()
    {

        itemSpriteDictionary = new Dictionary<String, Sprite>();

        int i = 0;

        foreach (ItemSpriteEntry entry in itemList)
        {
            itemSpriteDictionary.Add(entry.item, entry.image);

            Debug.Log(i + " - Key: " + itemSpriteDictionary.Keys.ToString());
            Debug.Log(i + " - Value: " + itemSpriteDictionary.Values);


            i++;
        }
    }

    public Sprite GetIcon(String item)
    {
        //Sprite icon = KeyByValue(itemSpriteLookup, item);

        Sprite icon = itemSpriteDictionary[item];


        return icon;
    }

    private Sprite KeyByValue(Dictionary<String, Sprite> _itemSpriteDictionary, String item)
    {
        Sprite iconToFind = null;

        Debug.Log(item);

        foreach (KeyValuePair<String, Sprite> pair in _itemSpriteDictionary)
        {
            Debug.Log("Pair: " + pair);

            if (pair.Key == item)
            {
                Debug.Log("itemFound");
                iconToFind = pair.Value;
                break;
            }
        }
        return iconToFind;
    }
}
