﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public IconsList iconsList;

    public GameObject gadgetSlot;
    public GameObject primaryWeaponSlot;
    public GameObject secondaryWeaponSlot;

    public Image gadgetImage;
    public RawImage primaryWeaponImage;
    public RawImage secondaryWeaponImage;

    void Start ()
    {
        gadgetSlot = GameObject.Find("Gadget");
        primaryWeaponSlot = GameObject.Find("PrimaryWeapon");
        secondaryWeaponSlot = GameObject.Find("SecondaryWeapon");

        gadgetImage = GameObject.Find("Gadget/Image").GetComponent<Image>();
        primaryWeaponImage = GameObject.Find("PrimaryWeapon/Image").GetComponent<RawImage>();
        secondaryWeaponImage = GameObject.Find("SecondaryWeapon/Image").GetComponent<RawImage>();
    }

    public void SetGadget(Sprite gadget)
    {
        // ******* Only for the IconsList.cs *******
        // gadgetImage.sprite = iconsList.GetIcon(gadget.name);

        gadgetImage.sprite = gadget;
    }

    public void SetWeapon()
    {

    }
}
