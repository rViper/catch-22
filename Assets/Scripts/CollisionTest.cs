﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTest : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collider)
    {
        if(collider.gameObject.tag == "Environment")
        {
            Debug.Log("Environment Collision!");
        }
    }
}
