﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour, IDamagable
{
    public int startingHealth;
    protected int health;

    protected virtual void Start()
    {
        health = startingHealth;
    }

    public virtual void PreRagDoll()
    {
        Rigidbody2D[] bodyParts = GetComponentsInChildren<Rigidbody2D>();

        foreach (Rigidbody2D rb2D in bodyParts)
        {
            rb2D.gravityScale = 0;
        }

        HingeJoint2D[] hingeJointParts = GetComponentsInChildren<HingeJoint2D>();

        foreach (HingeJoint2D hj2D in hingeJointParts)
        {
            hj2D.enabled = false;
        }
    }

    public virtual void TakeDamage(int damage, Vector2 direction, GameObject bodyPart)
    {
        health -= damage;
        Debug.Log(health);
        if (health <= 0)
        {
            Die(direction, bodyPart);
        }
    }

    public virtual void Die(Vector2 direction, GameObject bodyPart)
    {
        Destroy(this.gameObject);
    }

    public virtual void RagDoll(Vector2 direction, GameObject bodyPart)
    {
        Rigidbody2D[] bodyParts = GetComponentsInChildren<Rigidbody2D>();

        foreach (Rigidbody2D rb2D in bodyParts)
        {
            rb2D.gravityScale = 1;
        }

        HingeJoint2D[] hingeJointParts = GetComponentsInChildren<HingeJoint2D>();

        foreach (HingeJoint2D hj2D in hingeJointParts)
        {
            hj2D.enabled = true;
        }

        GetComponent<Animator>().enabled = false;

        Debug.Log(bodyPart);
        bodyPart.GetComponent<Rigidbody2D>().AddForce(direction * 1000);
    }
}
