﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineSound : MonoBehaviour
{

    FMOD.Studio.EventInstance machineSound;

    void OnTriggerEnter2D(Collider2D collision)
    {
        machineSound = SoundManager.Instance.PlayEvent("event:/FX/Noise/MachineNoise", transform.position);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        machineSound.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
