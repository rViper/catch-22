﻿using FMOD.Studio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour
{
    public static GameMaster GM;

    public bool dataAcquired;
    public int levelToLoad;

    private List<GameObject> interactuableItems;
    private Text message;
    private GameObject fadeIn;
    private GameObject toBeContinued;

    private OptionsEasing optionsMenu;
    private bool controlsActivated = false;

    [Space]
    [SerializeField] private GameObject player;
    [SerializeField] private Transform spawnPosition;


    void Awake()
    {
        if (GM != null)
            GameObject.Destroy(GM);
        else
            GM = this;

        //ackgroundSound = SoundManager.Instance.PlayEvent("event:/FX/Background/Level1", transform.position);
        //backgroundSound.start();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameMaster.GM.LaunchOptions();
        }

        // Main Menu
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
        // Tutorial
        else if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            SceneManager.LoadScene(2, LoadSceneMode.Single);
        }
        // Juego
        else if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            SceneManager.LoadScene(3, LoadSceneMode.Single);
            //backgroundSound.stop(STOP_MODE.ALLOWFADEOUT);
        }

    }

    void Start()
    {
       // message = GameObject.Find("HUD/Canvas/Message").GetComponent<Text>();
        fadeIn = GameObject.Find("FadeIn");
        if (fadeIn != null)
            fadeIn.SetActive(false);

        toBeContinued = GameObject.Find("CONTINUE");
        if (toBeContinued != null)
            toBeContinued.SetActive(false);

        optionsMenu = GameObject.Find("_Options").GetComponent<OptionsEasing>();

        Invoke("DeleteFadeOut", 2);
    }

    void DeleteFadeOut()
    {
        GameObject fadeOut = GameObject.Find("FadeOut");

        if (fadeOut != null)
            fadeOut.SetActive(false);
    }

    void FadeIn()
    {
        fadeIn.SetActive(true);

        Invoke("LoadLevel", 2.5f);
    }

    public void LaunchOptions()
    {
        controlsActivated = !controlsActivated;
        optionsMenu.Launch();
        Player.player.optionsEnabled = controlsActivated;
    }

    public void RespawnPlayer(GameObject player)
    {
        //GameObject newLia = Instantiate(player, spawnPosition.position, Quaternion.identity) as GameObject;
        //newLia.transform.position = spawnPosition.position;

        player.transform.position = spawnPosition.position;

        Debug.Log("TEST");
    }

    /***** PROVISIONAL ******/
    public void FadeMainMenu()
    {
        fadeIn.SetActive(true);

        Invoke("MainMenu", 2.5f);
    }
    /***** PROVISIONAL ******/
    void MainMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void SetDataAcquried(string _message, AudioSource sound, bool value)
    {
        dataAcquired = value;
        if (_message != null)
            message.text = _message;
        else
            message.text = "Data Acquired";

        sound.Play();

        Invoke("MessageSent", 3f);
    }

    void MessageSent()
    {
        message.text = "";
    }


    void LoadLevel()
    {
        if (levelToLoad != -1)
        {
            SceneManager.LoadScene(levelToLoad, LoadSceneMode.Single);
        }
        else
        {
            GameObject hud = GameObject.Find("HUD");
            hud.SetActive(false);

            toBeContinued.SetActive(true);
            levelToLoad = 0;

            Invoke("LoadLevel", 3.5f);
        }
    }

    void OnTriggerEnter2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            FadeIn();
        }
    }
}