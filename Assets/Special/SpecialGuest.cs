﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class SpecialGuest : MonoBehaviour {

    public bool specialThing = false;
    public bool alreadyPlaying = false;
    public AudioSource audio;
    public VideoPlayer video;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && specialThing)
        {
            if (!alreadyPlaying)
            {
                audio.Play();
                video.Play();
                alreadyPlaying = true;
            }
            else if(alreadyPlaying)
            {
                audio.Stop();
                video.Stop();
                alreadyPlaying = false;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            specialThing = true;
        }
    }

    void OnTriggerExit2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            specialThing = false;
        }
    }
}
